import requests
import shutil

# url = "https://docs.google.com/uc?id=1jr1VRWAQqnh9dqvbeHH9mLhhOJVGUaQ1&export=download"
url = "https://drive.google.com/uc?export=download&id=1SRYoik4r9rkPvXehbeOcoTb5NM4X_Hl_"

USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36'


# Download file using requests
# http://docs.python-requests.org/en/latest/

r = requests.get(url, stream=True)
with open('test1.exe', 'wb') as f:
	shutil.copyfileobj(r.raw, f)