import threading
import queue
import time
import sys
import win32gui
import win32api
import win32con
import re
from threading import Timer
import pywinauto.application
import likeyoubot_message
import likeyoubot_win
import likeyoubot_configure
import pickle
import likeyoubot_resource
from PIL import ImageGrab
import cv2
import numpy as np
from matplotlib import pyplot as plt
import operator
import random
import likeyoubot_lineage2revolution
import likeyoubot_clans
import likeyoubot_tera
import likeyoubot_yeolhyul
import likeyoubot_blackdesert
from likeyoubot_configure import LYBConstant as lybconstant
from datetime import datetime
import likeyoubot_logger
import traceback
import pyautogui

class LYBWorker(threading.Thread):
	def __init__(self, name, configure, cmd_queue, res_queue):
		super().__init__()
		self.logger = likeyoubot_logger.LYBLogger.getLogger()
		self.command_queue = cmd_queue
		self.response_queue = res_queue
		self.name = name
		self.keyword = ""
		self.start_action = False
		self.start_flag = 0x00
		self.ui = None
		self.hwnd = 0
		self.side_hwnd = 0
		self.parent_hwnd = 0
		self.multi_hwnd = 0
		self.multi_handle_dic = {}
		self.game = None
		self.game_tab = None
		self.game_name = None
		self.game_option = None
		self.win = None
		self.window_title = ''
		self.configure = configure
		self.config = None
		self.common_config = None
		self.window_config = None
		self.pause_flag = False
		self.app_player_type = 'nox'

	def run(self):
		threading.currentThread().setName('워커쓰레드')
		# logger.debug('['+self.name+']'+' start:'+str(threading.currentThread()))
		while True:
			try:
				if self.pause_flag == True:
					if self.game != None:
						self.game.interval = 9999999
					recv_msg = self.command_queue.get()
				else:
					recv_msg = self.command_queue.get_nowait()

				if recv_msg.type == 'end':
					self.response_queue.put_nowait(likeyoubot_message.LYBMessage('end_return', str(self.window_title)))
					self.response_queue.join()
					break
				elif recv_msg.type == 'start_app_player':
					self.logger.warn('WORKER: start_app_player')
					player_type = recv_msg.message[0]
					multi_hwnd_dic = recv_msg.message[1]
					window_title = recv_msg.message[2]
					configure = recv_msg.message[3]
					window_config = configure.window_config[window_title]
					window = likeyoubot_win.LYBWin(configure.window_title, configure)

					if player_type == 'nox':
						if lybconstant.LYB_MULTI_APP_PLAYER_NAME_NOX in multi_hwnd_dic:
							mHwnd = multi_hwnd_dic[lybconstant.LYB_MULTI_APP_PLAYER_NAME_NOX]
							app_player_index = int(window_config[lybconstant.LYB_DO_BOOLEAN_FIX_WINDOW_LOCATION + 'number']) - 1
							self.logger.debug('app_player_index: ' + str(app_player_index))

							window.mouse_click(mHwnd, 523, 116 + (57 * app_player_index))
					elif player_type == 'momo':
						if lybconstant.LYB_MULTI_APP_PLAYER_NAME_MOMO in multi_hwnd_dic:
							mHwnd = multi_hwnd_dic[lybconstant.LYB_MULTI_APP_PLAYER_NAME_MOMO]
							app_player_index = int(window_config[lybconstant.LYB_DO_BOOLEAN_FIX_WINDOW_LOCATION + 'number']) - 1
							self.logger.debug('app_player_index: ' + str(app_player_index))

							window.mouse_click(mHwnd, 387, 116 + (50 * app_player_index))

					self.response_queue.put_nowait(likeyoubot_message.LYBMessage('end_start_app_player', str(self.window_title)))
					self.response_queue.join()

					break
				elif recv_msg.type =='search':
					window_config = recv_msg.message					
					(handle_list, side_handle_dic, parent_handle_dic, multi_handle_dic) = self.findWindows()

					window_list, rhwnds_dic = self.set_location(window_config, handle_list, side_handle_dic, parent_handle_dic)

					self.logger.debug('search window handle list: ' + str(window_list))
					self.response_queue.put_nowait(likeyoubot_message.LYBMessage('search_hwnd_return', rhwnds_dic))
					self.response_queue.put_nowait(likeyoubot_message.LYBMessage('search_side_hwnd_return', side_handle_dic))
					self.response_queue.put_nowait(likeyoubot_message.LYBMessage('search_parent_hwnd_return', parent_handle_dic))
					self.response_queue.put_nowait(likeyoubot_message.LYBMessage('search_multi_hwnd_return', multi_handle_dic))
					self.response_queue.put_nowait(likeyoubot_message.LYBMessage('search_title_return', window_list))

					self.response_queue.join()
					break
				# elif recv_msg.type =='hide':
				# 	self.ui = recv_msg.message
				# 	self.logger.warn('창 숨기기')
				# 	self.win = likeyoubot_win.LYBWin(self.ui.configure.window_title, self.ui.configure)

				# 	if len(self.ui.parent_hwnds) > 0:
				# 		hwnds = self.ui.parent_hwnds
				# 	else:
				# 		hwnds = self.ui.hwnds

				# 	# (handle_list, side_handle_dic, parent_handle_dic, multi_handle_dic) = self.findWindows()
				# 	handle_list = []
				# 	for key, each_handle in self.ui.hwnds.items():
				# 		handle_list.append(each_handle)

				# 	self.logger.warn(handle_list)
				# 	parent_handle_dic = self.ui.parent_hwnds
				# 	side_handle_dic = self.ui.side_hwnds

				# 	window_list, rhwnds_dic = self.set_location(
				# 								self.ui.configure.window_config, 
				# 								handle_list, 
				# 								side_handle_dic, 
				# 								parent_handle_dic, 
				# 								custom_loc_x=self.ui.master.winfo_screenwidth(), 
				# 								custom_loc_y=self.ui.master.winfo_screenheight() )

				# 	for key, each_hwnd in hwnds.items():
				# 		self.win.setInvisible(int(each_hwnd))

				# 	self.response_queue.join()
				# 	break
				elif recv_msg.type =='watchout':
					self.ui = recv_msg.message[0]
					cmd = recv_msg.message[1]
					window_name = recv_msg.message[2]

					self.win = likeyoubot_win.LYBWin(self.ui.configure.window_title, self.ui.configure)


					if cmd == 'show':
						self.logger.warn('창 보이기')
						custom_loc_x=0
						custom_loc_y=0
					else:
						self.logger.warn('창 숨기기')
						custom_loc_x=self.ui.master.winfo_screenwidth()
						custom_loc_y=self.ui.master.winfo_screenheight()

					if window_name == None:
						# if len(self.ui.parent_hwnds) > 0:
						# 	hwnds = self.ui.parent_hwnds
						# else:
						# 	hwnds = self.ui.hwnds
							
						# self.ui.searchWindow(None)
						handle_list = []
						for key, each_handle in self.ui.hwnds.items():
							handle_list.append(each_handle)
							
						parent_handle_dic = self.ui.parent_hwnds
						side_handle_dic = self.ui.side_hwnds
					else:
						win_hwnds = self.ui.hwnds[window_name]
						handle_list = [ win_hwnds ]

						parent_handle_dic = {}
						try:
							parent_handle_dic[win_hwnds] = self.ui.parent_hwnds[win_hwnds]
						except:
							pass

						side_handle_dic = {}
						try:
							side_handle_dic[win_hwnds] = self.ui.side_hwnds[win_hwnds]
						except:
							pass

					window_list, rhwnds_dic = self.set_location(
												self.ui.configure.window_config, 
												handle_list, 
												side_handle_dic, 
												parent_handle_dic,
												custom_loc_x=custom_loc_x,
												custom_loc_y=custom_loc_y )

					if cmd == 'show':
						for each_hwnd in handle_list:
							if each_hwnd in self.ui.parent_hwnds:
								self.win.setVisible(self.ui.parent_hwnds[each_hwnd])
							else:
								self.win.setVisible(each_hwnd)
					else:
						for each_hwnd in handle_list:
							if each_hwnd in self.ui.parent_hwnds:
								self.win.setInvisible(self.ui.parent_hwnds[each_hwnd])
							else:
								self.win.setInvisible(each_hwnd)

					self.response_queue.join()
					break
				elif recv_msg.type == 'longPolling':
					self.ui = recv_msg.message
					threading.currentThread().setName('LongPollingWorker')
					self.logger.info('LongPollingWorker started')
					if self.win == None:
						self.win = likeyoubot_win.LYBWin(self.ui.configure.window_title, self.ui.configure)
				elif recv_msg.type == 'thumbnail':
					self.ui = recv_msg.message[0]
					window_name = recv_msg.message[1]
					# img = self.win.get_window_screenshot(self.multi_hwnd, 2)
					# img_np = np.array(img)
					# frame = cv2.cvtColor(img_np, cv2.COLOR_BGR2RGB)
					# cv2.imshow("test", frame)
					# cv2.waitKey(0)
					# cv2.destroyAllWindows()

					win_hwnds = self.ui.hwnds[window_name]
					self.win = likeyoubot_win.LYBWin(self.ui.configure.window_title, self.ui.configure)
					(anchor_x, anchor_y, end_x, end_y) = self.win.get_window_location(win_hwnds)
					adj_x, adj_y = self.win.get_player_adjust(win_hwnds)
					width = int(self.ui.configure.common_config[lybconstant.LYB_DO_STRING_THUMBNAIL_SIZE + 'width'])
					height = int(self.ui.configure.common_config[lybconstant.LYB_DO_STRING_THUMBNAIL_SIZE + 'height'])
					is_shortcut = self.ui.configure.common_config[lybconstant.LYB_DO_STRING_THUMBNAIL_SIZE + 'shortcut']
					while(True):

						win_hwnds = self.ui.hwnds[window_name]
						# img = ImageGrab.grab(bbox=(anchor_x - adj_x, anchor_y - adj_y, end_x, end_y))
						try:
							img = self.win.get_window_screenshot(win_hwnds, 2)
							# img = ImageGrab.grab(bbox=(100,10,400,780)) #bbox specifies specific region (bbox= x,y,width,height)
						except:
							# self.logger.error(traceback.format_exc())
							pass

						img_np = np.array(img)
						img_np = cv2.resize(img_np, (width, height), interpolation = cv2.INTER_AREA)
						frame = cv2.cvtColor(img_np, cv2.COLOR_BGR2RGB)
						if is_shortcut == True:
							title = "Press ESC or Q " + str(win_hwnds)
						else:
							title = "DogFooter " + str(win_hwnds)
						cv2.imshow(title, frame)
						wait_key = cv2.waitKey(25)

						if is_shortcut == True:
							if wait_key & 0xFF == ord('q'):
								break
							elif wait_key == 27:
								break

						if cv2.getWindowProperty(title, 0) == -1:
							break

					cv2.destroyAllWindows()
					break
				elif recv_msg.type == 'start':
					self.start_action = True
					self.start_flag = recv_msg.message[0]
					self.hwnd = recv_msg.message[1]
					self.window_title = recv_msg.message[2]
					self.game_name = recv_msg.message[3]
					self.game_option = recv_msg.message[4]
					self.config = recv_msg.message[5]
					self.common_config = self.config.common_config
					self.window_config = recv_msg.message[6]
					self.side_hwnd = recv_msg.message[7]
					self.parent_hwnd =  recv_msg.message[8]
					self.multi_handle_dic =  recv_msg.message[9]
					self.game_tab = recv_msg.message[10]

					threading.currentThread().setName(self.window_title)

					if self.win == None:
						self.win = likeyoubot_win.LYBWin(self.configure.window_title, self.configure)
					if self.window_config[lybconstant.LYB_DO_BOOLEAN_USE_INACTIVE_MODE] == False:
						self.win.set_foreground(self.hwnd)


					# 무슨 게임이냐에 따라서
					try:

						if self.game_name == lybconstant.LYB_GAME_LIN2REV:
							self.game = likeyoubot_lineage2revolution.LYBLineage2Revolution(None, None, self.win)
						elif self.game_name == lybconstant.LYB_GAME_CLANS:
							self.game = likeyoubot_clans.LYBClans(None, None, self.win)
						elif self.game_name == lybconstant.LYB_GAME_TERA:
							self.game = likeyoubot_tera.LYBTera(None, None, self.win)
						elif self.game_name == lybconstant.LYB_GAME_YEOLHYUL:
							self.game = likeyoubot_yeolhyul.LYBYeolhyul(None, None, self.win)
						elif self.game_name == lybconstant.LYB_GAME_BLACKDESERT:
							self.game = likeyoubot_blackdesert.LYBBlackDesert(None, None, self.win)


						self.game.setGameTab(self.game_tab)
						self.game.setLoggingQueue(self.response_queue)
						self.game.setCommonConfig(self.config)
						self.game.setWindowConfig(self.window_config)
						self.game.setWindowHandle(self.hwnd, self.side_hwnd, self.parent_hwnd, self.multi_handle_dic)
						
					except:
						self.logger.error(traceback.format_exc())
						# self.response_queue.put_nowait(likeyoubot_message.LYBMessage('log', 'Thread Game Init Exception:' +  str(sys.exc_info()[0]) + '(' +str(sys.exc_info()[1]) + ')'))
						self.response_queue.put_nowait(likeyoubot_message.LYBMessage('end_return', 'Fail to initialize'))
						self.response_queue.join()
						break

					self.logger.info('[' + self.window_title + '] 창, [' + self.game_name + '] 게임 작업 시작')
					# self.response_queue.put_nowait(
					# 	likeyoubot_message.LYBMessage('log', 
					# 		'[' + self.window_title + '] 창에서 [' + self.game_name + '] 게임에 대해 작업을 시작합니다')
					# 	)

					self.app_player_type = self.win.get_player(self.hwnd)
					win_width, win_height = self.win.get_player_size(self.hwnd)

					# print(win_width, win_height)
					if self.app_player_type == 'momo':
						
						self.response_queue.put_nowait(
							likeyoubot_message.LYBMessage('log',
								'[' + self.window_title + '] 창 크기: ' + str((win_width, win_height)) +', 플레이어 종류: '+'모모')
							)
						if (	self.window_config[lybconstant.LYB_DO_STRING_INACTIVE_MODE_FLAG] == '윈7' or
								self.window_config[lybconstant.LYB_DO_BOOLEAN_USE_INACTIVE_MODE] == False):
							self.logger.warn('앱 플레이어 재시작 기능은 Windows 10 비활성 모드 필수')
						else:
							if lybconstant.LYB_MULTI_APP_PLAYER_NAME_MOMO in self.multi_handle_dic:
								self.multi_hwnd = self.multi_handle_dic[lybconstant.LYB_MULTI_APP_PLAYER_NAME_MOMO]
								self.logger.critical(str(lybconstant.LYB_MULTI_APP_PLAYER_NAME_MOMO) + '(' + str(self.multi_hwnd) +') 검색됨')
								self.logger.critical('앱 플레이어 재시작 기능 사용 가능')

							# img = self.win.get_window_screenshot(self.multi_hwnd, 2)
							# img_np = np.array(img)
							# frame = cv2.cvtColor(img_np, cv2.COLOR_BGR2RGB)
							# cv2.imshow("test", frame)
							# cv2.waitKey(0)
							# cv2.destroyAllWindows()
	

							# (anchor_x, anchor_y, end_x, end_y) = self.win.get_window_location(self.hwnd)
							# adj_x, adj_y = self.win.get_player_adjust(self.hwnd)
							# self.logger.warn('CP1')
							# while(True):
							# 	# img = ImageGrab.grab(bbox=(anchor_x - adj_x, anchor_y - adj_y, end_x, end_y))
							# 	img = self.win.get_window_screenshot(self.hwnd, 2)
							# 	# img = ImageGrab.grab(bbox=(100,10,400,780)) #bbox specifies specific region (bbox= x,y,width,height)

							# 	img_np = np.array(img)
							# 	img_np = cv2.resize(img_np, (64, 36), interpolation = cv2.INTER_AREA)
							# 	frame = cv2.cvtColor(img_np, cv2.COLOR_BGR2RGB)
							# 	cv2.imshow("test", frame)
							# 	if cv2.waitKey(25) & 0xFF == ord('q'):
							# 		break

							# cv2.destroyAllWindows()
							# self.logger.warn('CP2')

					elif self.app_player_type == 'nox':
						
						self.response_queue.put_nowait(
							likeyoubot_message.LYBMessage('log',
								'[' + self.window_title + '] 창 크기: ' + str((win_width, win_height)) +', 플레이어 종류: '+'녹스')
							)

					else:
						self.response_queue.put_nowait(
							likeyoubot_message.LYBMessage('log',
								'[' + self.window_title + '] 창 크기: ' + str((win_width, win_height)) +' - (640, 360) 불일치')
							)
						self.response_queue.put_nowait(likeyoubot_message.LYBMessage('end_return', 'Fail to initialize'))
						self.response_queue.join()
						break

					self.game.setAppPlayer(self.app_player_type)

				elif recv_msg.type == 'pause':
					if self.pause_flag == True:
						if self.game != None:
							self.game.interval = None
						self.logger.warn("Resume")
						self.pause_flag = False
					else:
						self.logger.warn("Paused")
						self.pause_flag = True

			except queue.Empty:
				pass
			except:
				self.logger.error(traceback.format_exc())
				self.response_queue.put_nowait(likeyoubot_message.LYBMessage('end_return', str(traceback.format_exc())))
				self.response_queue.join()
				break

			try:
				if self.start_action:
					s = time.time()
					rc = self.letsgetit()				
					if rc < 0:
						self.response_queue.put_nowait(likeyoubot_message.LYBMessage('end_return', self.window_title + ' 비정상'))
						break
					elif rc == 7000051:
						self.logger.warn('DEBUG CP - 1')
						self.response_queue.put_nowait(likeyoubot_message.LYBMessage('stop_app', self.game))
						self.response_queue.join()
						self.game.interval = int(self.common_config[lybconstant.LYB_DO_BOOLEAN_USE_RESTART_APP_PLAYER + 'delay']) - 1
					else:
						self.response_queue.put_nowait(likeyoubot_message.LYBMessage('game_object', self.game))
					e = time.time()
					# print('[DEBUG] Process Game:', round(e-s, 2))
				elif self.ui != None:
					rc = self.longPolling()
					if rc < 0:
						self.logger.error('LongPollingWorker is terminated abnormally.')
						break
			except:
				self.logger.error(traceback.format_exc())
 				# self.logger.error(str(sys.exc_info()[0]) + '(' +str(sys.exc_info()[1]) + ')')
				self.response_queue.put_nowait(likeyoubot_message.LYBMessage('end_return', str(traceback.format_exc())))
				self.response_queue.join()
				break

			if self.game != None and self.game.interval != None:
				# print('[GAME INTERVAL]:', self.game.interval)
				if self.game.interval > 0:
					time.sleep(self.game.interval)
				self.game.interval = None
			else:
				if self.common_config == None:
					time.sleep(1)
				else:
					# print('[INTERVAL]:', float(self.common_config['wakeup_period_entry']))
					time.sleep(float(self.common_config['wakeup_period_entry']))
		# logger.debug('['+self.name+']'+' end:'+str(threading.currentThread()))

	def findWindows(self):

		self.win = likeyoubot_win.LYBWin(self.configure.window_title, self.configure)

		wildcard = ".*"+self.configure.keyword+".*"
		# wildcard = '.*'
		# print('DEBUG 1003:', wildcard)

		self.win.find_window_wildcard(wildcard)

		# print('DEBUG 1004:', self.win.handle_list)

		for each_hwnd in self.win.handle_list:
			# self.win.set_foreground(each_hwnd)
			time.sleep(0.1)
		time.sleep(0.1)
		# self.win.set_foreground(self.win.my_handle)

		return (self.win.handle_list, self.win.side_window_dic, self.win.parent_handle_dic, self.win.multi_window_handle_dic)
		# return (self.win.handle_list, {})



	def letsgetit(self):
		logging = '';

		(anchor_x, anchor_y, end_x, end_y) = self.win.get_window_location(self.hwnd)
		adj_x, adj_y = self.win.get_player_adjust(self.hwnd)
		cur_x, cur_y = pyautogui.position()

		self.game.cursor_loc = (cur_x - anchor_x + adj_x, cur_y - anchor_y + adj_x)
		self.game.statistics['마우스 포인터 위치'] = (cur_x - anchor_x + adj_x, cur_y - anchor_y + adj_x)

		# print('inactive mode flag =', self.window_config[lybconstant.LYB_DO_STRING_INACTIVE_MODE_FLAG])
		if self.window_config[lybconstant.LYB_DO_STRING_INACTIVE_MODE_FLAG] == '윈7':
			inactive_flag = 1
		else:
			inactive_flag = 2

		# print( self.app_player_type, (anchor_x, anchor_y, end_x, end_y) )
		# print('START GRAB', datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3])
		# s = time.time()
		try:
			if self.window_config[lybconstant.LYB_DO_BOOLEAN_USE_INACTIVE_MODE] == False:
				current_window_image_grab = ImageGrab.grab(bbox=(anchor_x - adj_x, anchor_y - adj_y, end_x, end_y))
			else:
				current_window_image_grab = self.win.get_window_screenshot(self.hwnd, inactive_flag)
		except:
			self.logger.error(traceback.format_exc())
			return 0
		# e = time.time()
		# print('[DEBUG] Grab Screenshot:', round(e-s,2))
		# print('END GRAB', datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3])
		# current_window_image_grab.save("test.png")



		# time.sleep(2)
		# sys.exit()

		return self.game.process(current_window_image_grab)


		# self.resource_manager.pixel_box_dic.debug()
		# print(self.resource_manager.pixel_box_dic['lineage_revolution_icon'][0])
		# ((loc_x, loc_y), (pixel_R, pixel_G, pixel_B)) = self.resource_manager.pixel_box_dic['lineage_revolution_icon'][0]
		# self.win.mouse_click(self.hwnd, int(loc_x), int(loc_y))
		# logging = 'Click (' + str(loc_x) + ', ' + str(loc_y) + '):'
		#self.win.mouse_drag(self.hwnd, 400, 180, 450, 180)

	def longPolling(self):

		try:
			self.ui.update_telegram()
			self.ui.check_ip()

			# f2 = win32api.GetAsyncKeyState(win32con.VK_F2)
			# shift = win32api.GetAsyncKeyState(win32con.VK_SHIFT)
			# e = win32api.GetAsyncKeyState(ord('E'))

			# if len(self.ui.parent_hwnds) > 0:
			# 	hwnds = self.ui.parent_hwnds
			# else:
			# 	hwnds = self.ui.hwnds

			# # if f2 != 0 and shift != 0:
			# if self.ui.show_window == True:
			# 	self.logger.warn('창 보이기')
			# 	self.ui.searchWindow(None)
			# 	for key, each_hwnd in hwnds.items():
			# 		self.win.setVisible(each_hwnd)
			# 	self.ui.show_window = False

			# # elif f2 != 0:
			# elif self.ui.hide_window == True:
			# 	self.logger.warn('창 숨기기')
						
			# 	(handle_list, side_handle_dic, parent_handle_dic, multi_handle_dic) = self.findWindows()
			# 	window_list, rhwnds_dic = self.set_location(
			# 								self.ui.configure.window_config, 
			# 								handle_list, 
			# 								side_handle_dic, 
			# 								parent_handle_dic, 
			# 								custom_loc_x=self.ui.master.winfo_screenwidth(), 
			# 								custom_loc_y=self.ui.master.winfo_screenheight() )
			# 	for key, each_hwnd in hwnds.items():
			# 		self.win.setInvisible(int(each_hwnd))
			
			# 	self.ui.hide_window = False
		except:
			# self.ui.show_window = False
			# self.ui.hide_window = False
			self.logger.error(traceback.format_exc())

		return 0

	def set_location(self, window_config, handle_list, side_handle_dic, parent_handle_dic, custom_loc_x=0, custom_loc_y=0):

		rhwnds_dic = {}
		window_list = []

		iterator = 0
		# self.logger.warn(str((custom_loc_x, custom_loc_y)))
		for h in handle_list:
			if h in parent_handle_dic:
				win_title = self.win.get_title(parent_handle_dic[h])
				try:
					if window_config[win_title][lybconstant.LYB_DO_BOOLEAN_FIX_WINDOW_LOCATION + 'boolean'] == True:
						try:
							win_loc_x = int(window_config[win_title][lybconstant.LYB_DO_BOOLEAN_FIX_WINDOW_LOCATION + 'x']) + custom_loc_x
							win_loc_y =	int(window_config[win_title][lybconstant.LYB_DO_BOOLEAN_FIX_WINDOW_LOCATION + 'y']) + custom_loc_y
						except:
							win_loc_x = custom_loc_x
							win_loc_y = custom_loc_y

						self.win.set_window_pos(parent_handle_dic[h], win_loc_x, win_loc_y)
				except:
					pass
			else:
				win_title = self.win.get_title(h)	
				try:
					if window_config[win_title][lybconstant.LYB_DO_BOOLEAN_FIX_WINDOW_LOCATION + 'boolean'] == True:
						try:
							win_loc_x = int(window_config[win_title][lybconstant.LYB_DO_BOOLEAN_FIX_WINDOW_LOCATION + 'x']) + custom_loc_x
							win_loc_y =	int(window_config[win_title][lybconstant.LYB_DO_BOOLEAN_FIX_WINDOW_LOCATION + 'y']) + custom_loc_y
						except:
							win_loc_x = custom_loc_x
							win_loc_y = custom_loc_y

						self.win.set_window_pos(h, win_loc_x, win_loc_y)
				except:
					pass

			if h in side_handle_dic:
				win_title = self.win.get_title(h)	
				try:
					if window_config[win_title][lybconstant.LYB_DO_BOOLEAN_FIX_WINDOW_LOCATION + 'boolean'] == True:
						win_width, win_height = self.win.get_player_size(h)
						try:		
							win_loc_x = int(window_config[win_title][lybconstant.LYB_DO_BOOLEAN_FIX_WINDOW_LOCATION + 'x']) + win_width + 4 + custom_loc_x
							win_loc_y =	int(window_config[win_title][lybconstant.LYB_DO_BOOLEAN_FIX_WINDOW_LOCATION + 'y']) + 30 + custom_loc_y
						except:								
							win_loc_x = win_width + 4 + custom_loc_x
							win_loc_y = 30 + custom_loc_y

						self.win.set_window_pos(side_handle_dic[h], win_loc_x, win_loc_y)
				except:
					pass


			window_list.append(win_title)
			rhwnds_dic[win_title] = h

			win_width, win_height = self.win.get_player_size(h)
			iterator += 1
			# time.sleep(1)

		return window_list, rhwnds_dic

