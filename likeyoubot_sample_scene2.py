import likeyoubot_resource as lybrsc
import likeyoubot_message
import cv2
import sys
import numpy as np
from matplotlib import pyplot as plt
import pyautogui
import operator
import random
import likeyoubot_game as lybgame
import likeyoubot_swgoh as lybgameswgoh
from likeyoubot_configure import LYBConstant as lybconstant
import likeyoubot_scene
import time

class LYBSwgohScene(likeyoubot_scene.LYBScene):
	def __init__(self, scene_name):
		likeyoubot_scene.LYBScene.__init__(self, scene_name)

	def process(self, window_image, window_pixels):

		super(LYBSwgohScene, self).process(window_image, window_pixels)

		rc = 0
		if self.scene_name == 'init_screen_scene':
			rc = self.init_screen_scene()
		elif self.scene_name == 'main_scene':
			rc = self.main_scene()
		elif self.scene_name == 'ilil_mission_scene':
			rc = self.ilil_mission_scene()
		elif self.scene_name == 'light_side_scene':
			rc = self.light_side_scene()
		elif self.scene_name == 'light_bunde_select_scene':
			rc = self.light_bunde_select_scene()
		elif self.scene_name == 'jeontoo_scene':
			rc = self.jeontoo_scene()
		elif self.scene_name == 'jadong_jeontoo_scene':
			rc = self.jadong_jeontoo_scene()
		elif self.scene_name == 'kantina_scene':
			rc = self.kantina_scene()
		elif self.scene_name == 'jungrip_bunde_select_scene':
			rc = self.jungrip_bunde_select_scene()
		elif self.scene_name == 'jiwon_yocheong_scene':
			rc = self.jiwon_yocheong_scene()
		elif self.scene_name == 'defeat_scene':
			rc = self.defeat_scene()
		elif self.scene_name == 'inventory_scene':
			rc = self.inventory_scene()
		elif self.scene_name == 'character_scene':
			rc = self.character_scene()
		elif self.scene_name == 'farming_place_scene':
			rc = self.farming_place_scene()
		elif self.scene_name == 'kantina_energy_scene':
			rc = self.kantina_energy_scene()
		elif self.scene_name == 'crystal_init_scene':
			rc = self.crystal_init_scene()
		elif self.scene_name == 'dark_side_scene':
			rc = self.dark_side_scene()
		elif self.scene_name == 'dark_bunde_select_scene':
			rc = self.dark_bunde_select_scene()

			



		else:
			rc = self.else_scene()

		return rc

	def else_scene(self):

		if self.status == 0:
			self.logger.warn('unknown_scene: ' + self.scene_name)
			self.status += 1
		else:
			if self.scene_name + '_close_icon' in self.game_object.resource_manager.pixel_box_dic:
				self.lyb_mouse_click(self.scene_name + '_close_icon')
				
			self.status = 0

		return self.status



	def crystal_init_scene(self):

		self.set_option('show', True)
		self.lyb_mouse_click(self.scene_name + '_close_icon', custom_threshold=0)
				
		return self.status

	def kantina_energy_scene(self):

		self.set_option('show', True)
		self.lyb_mouse_click(self.scene_name + '_close_icon', custom_threshold=0)
				
		return self.status

	def farming_place_scene(self):

		if self.status == 0:
			self.set_option('current_page', 0)
			self.set_option('drag_count', 0)
			self.set_option('index', 0)
			self.status += 1
		elif self.status == 1:
			index = self.get_option('index')
			self.logger.warn("DEBUG: " + str(index))
			if index > 2:
				self.set_option('index', 0)

				current_page = self.get_option('current_page')
				self.set_option('current_page', current_page + 1)

				self.set_option('drag_count', current_page)

				self.status += 1
			else:
				pb_name = 'farming_place_scene_jeontoo'
				(loc_x, loc_y),  match_rate = self.game_object.locationOnWindowPart(
								self.window_image,
								self.game_object.resource_manager.pixel_box_dic[pb_name],
								custom_threshold=0.8,
								custom_flag=1,
								custom_rect=(20 + 160*index, 300, 200 + 200*index, 380)
								)
				self.logger.warn(match_rate)
				if loc_x != -1:
					self.lyb_mouse_click_location(loc_x, loc_y)
					self.set_option('drag_count', 0)
					self.status += 1
				self.set_option('index', index + 1)
		elif self.status == 2:
			current_page = self.get_option('current_page')
			drag_count = self.get_option('drag_count')
			self.logger.warn('DEBUG --- ' + str(drag_count) + ' < ' + str(current_page))
			if current_page > 2:
				self.lyb_mouse_click('back', custom_threshold=0)
				self.game_object.get_scene('character_scene').set_option('is_done', True)
				self.status = 0
			else:
				for i in range(drag_count, current_page + 1):
					time.sleep(1)
					self.lyb_mouse_drag('farming_place_scene_drag_right', 'farming_place_scene_drag_left')
				self.game_object.interval = self.period_bot(1)
				self.status -= 1
		else:
			self.lyb_mouse_click(self.scene_name + '_close_icon')	
			self.status = 0

		return self.status

	def character_scene(self):

		if self.status == 0:
			self.set_option('is_done', False)
			self.status += 1
		elif self.status == 1:
			if self.get_option('is_done') == False:
				self.lyb_mouse_click('character_scene_chatgi', custom_threshold=0)
				self.game_object.get_scene('farming_place_scene').status = 0
			else:
				iterator = self.game_object.get_scene('inventory_scene').get_option('iterator')
				if iterator == None:
					iterator = 0

				self.game_object.get_scene('inventory_scene').set_option('iterator', iterator + 1)
				self.lyb_mouse_click('back', custom_threshold=0)
				self.status = 0
		else:
			self.lyb_mouse_click(self.scene_name + '_close_icon')	
			self.status = 0

		return self.status

	def inventory_scene(self):

		if self.status == 0:
			self.set_option('iterator', 0)
			self.status += 1
		elif self.status == 1:
			iterator = self.get_option('iterator')
			if iterator > 11:
				self.status = 99999
			else:
				self.lyb_mouse_click('inventory_scene_charater_location_' + str(iterator), custom_threshold=0)
				self.game_object.get_scene('character_scene').status = 0
		else:
			self.lyb_mouse_click(self.scene_name + '_close_icon')	
			self.status = 0

		return self.status

	def defeat_scene(self):

		if self.status == 0:
			self.status += 1
		else:
			if self.scene_name + '_close_icon' in self.game_object.resource_manager.pixel_box_dic:
				self.lyb_mouse_click(self.scene_name + '_close_icon')
				
			self.status = 0

		return self.status

	def jiwon_yocheong_scene(self):

		if self.status == 0:
			self.status += 1
		elif self.status == 1:
			self.lyb_mouse_click('jiwon_yocheong_scene_list_0', custom_threshold=0)
		else:
			if self.scene_name + '_close_icon' in self.game_object.resource_manager.pixel_box_dic:
				self.lyb_mouse_click(self.scene_name + '_close_icon')
				
			self.status = 0

		return self.status

	def jungrip_bunde_select_scene(self):

		if self.status == 0:
			self.status += 1
		elif self.status == 1:
			self.game_object.get_scene('jiwon_yocheong_scene').status = 0
			self.lyb_mouse_click('jungrip_bunde_select_scene_jeontoo', custom_threshold=0)
		else:
			if self.scene_name + '_close_icon' in self.game_object.resource_manager.pixel_box_dic:
				self.lyb_mouse_click(self.scene_name + '_close_icon')
				
			self.status = 0

		return self.status


	def jadong_jeontoo_scene(self):
		return self.jeontoo_scene()

	def jeontoo_scene(self):


		if self.status == 0:
			self.game_object.get_scene('jeontoo_scene').set_option('start', True)
			self.status += 1
		elif self.status == 1:
			pb_name = 'sudong'
			match_rate = self.game_object.rateMatchedPixelBox(self.window_pixels, pb_name)
			if match_rate > 0.99:
				self.lyb_mouse_click(pb_name, custom_threshold=0)
			self.status = 0
		else:
			if self.scene_name + '_close_icon' in self.game_object.resource_manager.pixel_box_dic:
				self.lyb_mouse_click(self.scene_name + '_close_icon')
				
			self.status = 0

		return self.status

	def dark_bunde_select_scene(self):
		return self.light_bunde_select_scene()

	def light_bunde_select_scene(self):

		if self.status == 0:
			self.status += 1
		elif self.status == 1:
			self.lyb_mouse_click('light_bunde_select_scene_jeontoo', custom_threshold=0)
		else:
			if self.scene_name + '_close_icon' in self.game_object.resource_manager.pixel_box_dic:
				self.lyb_mouse_click(self.scene_name + '_close_icon')
				
			self.status = 0

		return self.status

	def kantina_scene(self):
		return self.light_side_scene()
		
	def dark_side_scene(self):
		return self.light_side_scene()

	def light_side_scene(self):

		if self.status == 0:
			self.status += 1
		elif self.status == 1:
			if self.game_object.get_scene('jeontoo_scene').get_option('start') == False:
				self.game_object.get_scene('jeontoo_scene').set_option('start', True)
				self.lyb_mouse_click('back', custom_threshold=0)
				self.status = 0
			else:
				self.lyb_mouse_click('light_side_scene_jeontoo', custom_threshold=0)
				self.game_object.get_scene('jeontoo_scene').set_option('start', False)
		else:
			if self.scene_name + '_close_icon' in self.game_object.resource_manager.pixel_box_dic:
				self.lyb_mouse_click(self.scene_name + '_close_icon')
				
			self.status = 0

		return self.status

	def ilil_mission_scene(self):

		if self.status == 0:
			self.status += 1
		elif self.status == 1:
			pb_name = 'mission_scene_receive'
			match_rate = self.game_object.rateMatchedPixelBox(self.window_pixels, pb_name)
			if match_rate > 0.7:
				self.lyb_mouse_click(pb_name, custom_threshold=0)
			else:
				self.status += 1
		else:
			if self.scene_name + '_close_icon' in self.game_object.resource_manager.pixel_box_dic:
				self.lyb_mouse_click(self.scene_name + '_close_icon')
				
			self.status = 0

		return self.status

	def init_screen_scene(self):
		
		self.schedule_list = self.get_game_config('schedule_list')
		if not '게임 시작' in self.schedule_list:
			return 0


		loc_x = -1
		loc_y = -1


		if self.game_object.player_type == 'nox':
			for each_icon in lybgameswgoh.LYBSwgoh.nox_swgoh_icon_list:
				(loc_x, loc_y),  match_rate = self.game_object.locationOnWindowPart(
								self.window_image,
								self.game_object.resource_manager.pixel_box_dic[each_icon],
								custom_threshold=0.8,
								custom_flag=1,
								custom_rect=(80, 110, 570, 300)
								)
				# self.logger.debug(match_rate)
				if loc_x != -1:
					self.lyb_mouse_click_location(loc_x, loc_y)
					break
		elif self.game_object.player_type == 'momo':
			for each_icon in lybgameswgoh.LYBSwgoh.nox_swgoh_icon_list:
				(loc_x, loc_y),  match_rate = self.game_object.locationOnWindowPart(
								self.window_image,
								self.game_object.resource_manager.pixel_box_dic[each_icon],
								custom_threshold=0.8,
								custom_flag=1,
								custom_rect=(30, 10, 610, 300)
								)
				# self.logger.debug(match_rate)
				if loc_x != -1:
					self.lyb_mouse_click_location(loc_x, loc_y)
					break

		# if loc_x == -1:
		# 	self.loggingToGUI('테라 아이콘 발견 못함')

		return 0


	def main_scene(self):

		if self.game_object.current_schedule_work != self.current_work:
			self.game_object.current_schedule_work = self.current_work

		self.game_object.main_scene = self

		self.schedule_list = self.get_game_config('schedule_list')
		if len(self.schedule_list) == 1:
			self.logger.warn('스케쥴 작업이 없어서 종료합니다.')
			return -1

		if self.status == 0:
			self.status += 1
		elif self.status >= 1 and self.status < 1000:

			self.set_schedule_status()

		elif self.status == self.get_work_status('미션'):

			if self.get_option(self.current_work + '_end_flag') == True:
				self.set_option(self.current_work + '_end_flag', False)
				self.set_option(self.current_work + '_inner_status', None)
				self.status = self.last_status[self.current_work] + 1
				return self.status

			elapsed_time = self.get_elapsed_time()
			if elapsed_time > self.period_bot(60):
				self.set_option(self.current_work + '_end_flag', True)
			else:
				inner_status = self.get_option(self.current_work + '_inner_status')
				if inner_status == None:
					inner_status = 0

				if inner_status == 0:
					self.lyb_mouse_click('main_scene_mission', custom_threshold=0)
				elif inner_status == 1:
					self.lyb_mouse_click('main_scene_light_side', custom_threshold=0)
					self.game_object.get_scene('light_side_scene').status = 0

				self.set_option(self.current_work + '_inner_status', inner_status + 1)

		elif self.status == self.get_work_status('조각 찾기'):

			if self.get_option(self.current_work + '_end_flag') == True:
				self.set_option(self.current_work + '_end_flag', False)
				self.set_option(self.current_work + '_inner_status', None)
				self.status = self.last_status[self.current_work] + 1
				return self.status

			elapsed_time = self.get_elapsed_time()
			if elapsed_time > self.period_bot(60):
				self.set_option(self.current_work + '_end_flag', True)
			else:
				self.lyb_mouse_click('main_scene_character', custom_threshold=0)
				self.game_object.get_scene('inventory_scene').status = 0


		elif self.status == self.get_work_status('알림'):

			try:
				self.game_object.telegram_send(str(self.get_game_config(lybconstant.LYB_DO_STRING_NOTIFY_MESSAGE)))
				self.status = self.last_status[self.current_work] + 1
			except:
				recovery_count = self.get_option(self.current_work + 'recovery_count')
				if recovery_count == None:
					recovery_count = 0

				if recovery_count > 2:
					self.status = self.last_status[self.current_work] + 1
					self.set_option(self.current_work + 'recovery_count', 0)
				else:
					self.logger.error(traceback.format_exc())
					self.set_option(self.current_work + 'recovery_count', recovery_count + 1)

		elif self.status == self.get_work_status('[작업 예약]'):

			self.logger.warn('[작업 예약]')
			self.game_object.wait_for_start_reserved_work = False
			self.status = self.last_status[self.current_work] + 1

		elif self.status == self.get_work_status('[작업 대기]'):
			elapsed_time = self.get_elapsed_time()
			limit_time = int(self.get_game_config(lybconstant.LYB_DO_STRING_WAIT_FOR_NEXT))
			if elapsed_time > limit_time:
				self.set_option(self.current_work + '_end_flag', True)
			else:
				self.loggingElapsedTime('[작업 대기]', int(elapsed_time), limit_time, period=10)

			if self.get_option(self.current_work + '_end_flag') == True:
				self.set_option(self.current_work + '_end_flag', False)
				self.status = self.last_status[self.current_work] + 1
				return self.status

		elif self.status == self.get_work_status('[반복 시작]'):

			self.set_option('loop_start', self.last_status[self.current_work])
			self.status = self.last_status[self.current_work] + 1

		elif self.status == self.get_work_status('[반복 종료]'):

			loop_count = self.get_option('loop_count')
			if loop_count == None:
				loop_count = 1

			self.logger.debug('[반복 종료] ' + str(loop_count) + ' 회 수행 완료, ' +
			 str(int(self.get_game_config(lybconstant.LYB_DO_STRING_COUNT_LOOP)) - loop_count) + ' 회 남음')
			if loop_count >= int(self.get_game_config(lybconstant.LYB_DO_STRING_COUNT_LOOP)):
				self.status = self.last_status[self.current_work] + 1
				self.set_option('loop_count', 1)
				self.set_option('loop_start', None)
			else:
				self.status = self.get_option('loop_start')
				# print('DEBUG LOOP STATUS = ', self.status )

				if self.status == None:
					self.logger.debug('[반복 시작] 점을 찾지 못해서 다음 작업을 수행합니다')
					self.status = self.last_status[self.current_work] + 1

				self.set_option('loop_count', loop_count + 1)

		else:
			self.status = self.last_status[self.current_work] + 1


		return self.status
















	def get_work_status(self, work_name):
		if work_name in lybgameswgoh.LYBSwgoh.work_list:
			return (lybgameswgoh.LYBSwgoh.work_list.index(work_name) + 1) * 1000
		else: 
			return 99999