# -*- mode: python -*-

block_cipher = None

a = Analysis(['likeyoubot_main.py'],
             pathex=['C:\\workspace\\dogfooter'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)

a.datas += [ ('lineage2_revolution.lyb', '.\\lineage2_revolution.lyb', 'DATA')]
a.datas += [ ('clans.lyb', '.\\clans.lyb', 'DATA')]
a.datas += [ ('tera.lyb', '.\\tera.lyb', 'DATA')]
a.datas += [ ('yeolhyul.lyb', '.\\yeolhyul.lyb', 'DATA')]
a.datas += [ ('blackdesert.lyb', '.\\blackdesert.lyb', 'DATA')]
a.datas += [ ('license_key.dat', '.\\license_key.dat', 'DATA')]
a.datas += [ ('license.dat', '.\\license.dat', 'DATA')]
a.datas += [ ('t_logo.png', '.\\t_logo.png', 'DATA')]

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='dogfooterbot',
          debug=False,
          strip=False,
          upx=True,
          console=False )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='dogfooterbot')
