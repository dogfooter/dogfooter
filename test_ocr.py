import sys
import numpy as np
import cv2
from PIL import Image

im = cv2.imread('test_ocr.png')
# height, width = im3.shape[:2]
# im = cv2.resize(im3, (int(width*3), int(height*3)))

gray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(gray,(5,5),0)
thresh = cv2.adaptiveThreshold(gray,255,1,1,11,2)
cv2.imwrite('test_ocr_conv.png', thresh)
# Image.fromarray(gray, 'RGB').save('testest.png')	

image, contours, hierachy = cv2.findContours(thresh, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

samples =  np.empty((0,100))

try:
	samples = np.loadtxt('generalsamples.data',np.float32)
except:
	samples =  np.empty((0,100))

try:
	responses = np.loadtxt('generalresponses.data',np.float32).tolist()
except:
	responses = []

print('samples: ', len(samples))
print('responses: ', len(responses))

keys = [i for i in range(48,58)]

print(len(contours))
for cnt in contours:
	print('CP1', cv2.contourArea(cnt))
	if cv2.contourArea(cnt) > 10 and cv2.contourArea(cnt) < 50:
		[x,y,w,h] = cv2.boundingRect(cnt)
		print('CP2', [x,y,w,h])

		if h > 5:
			cv2.rectangle(im,(x,y),(x+w,y+h),(0,0,255),2)
			roi = thresh[y:y+h,x:x+w]
			roismall = cv2.resize(roi,(10,10))
			cv2.imshow('norm',im)
			key = cv2.waitKey(0)
			print('key:', key)
			if key == 27:  # (escape to quit)
				sys.exit()
			elif key in keys:
				responses.append(int(chr(key)))
				sample = roismall.reshape((1,100))
				samples = np.append(samples,sample,0)


responses = np.array(responses,np.float32)
responses = responses.reshape((responses.size,1))
print("training complete")

np.savetxt('generalsamples.data',samples)
np.savetxt('generalresponses.data',responses)