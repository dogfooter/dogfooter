import tkinter
from tkinter import ttk
from tkinter import font
import threading
import time
import collections
import likeyoubot_worker
import queue
import pickle
import sys
import likeyoubot_gui
import os
import likeyoubot_message
import likeyoubot_lineage2revolution as LYBLIN2REV
import likeyoubot_clans as LYBCLANS
import likeyoubot_tera as LYBTERA
import likeyoubot_yeolhyul as LYBYEOLHYUL
from likeyoubot_configure import LYBConstant as lybconstant
import copy
import webbrowser

class LYBLoginGUI:

	def __init__(self, master, configure):
		self.master = master
		self.width = master.winfo_width()
		self.height = master.winfo_height()
		self.configure = configure
		self.master.title(self.configure.window_title + ' ' + str(lybconstant.LYB_VERSION))
		self.option_dic = {}
		self.gui_style = ttk.Style()
		print(self.gui_style.theme_names())

		self.gui_style.theme_use('vista')
		self.gui_style.configure('.', font=lybconstant.LYB_FONT)
		self.gui_style.configure("Tab", focuscolor=self.gui_style.configure(".")["background"])
		self.gui_style.configure("TButton", focuscolor=self.gui_style.configure(".")["background"])
		self.gui_style.configure("TCheckbutton", focuscolor=self.gui_style.configure(".")["background"])


		self.main_frame = ttk.Frame(master)
		label_frame = ttk.LabelFrame(self.main_frame, text='로그인')
		frame_extra = ttk.Frame(label_frame)
		frame_extra.pack(pady=5)
		frame_top = ttk.Frame(label_frame)
		frame_left = ttk.Frame(frame_top)
		frame = ttk.Frame(frame_left)
		label = ttk.Label(
			master 				= frame,
			text 				= "아이디",
			justify 			= tkinter.LEFT,
			width 				= 10
			)
		label.pack(side=tkinter.LEFT)

		self.option_dic[lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT + '_id'] = tkinter.StringVar(frame)
		if not lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT + '_id' in self.configure.common_config:
			self.configure.common_config[lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT + '_id'] = ''
		self.option_dic[lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT + '_id'].set(self.configure.common_config[lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT + '_id'])

		entry = ttk.Entry(
			master 				= frame,
			textvariable 		= self.option_dic[lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT + '_id'],
			width 				= 24
			)
		entry.pack(side=tkinter.LEFT, padx=2)
		frame.pack()

		frame = ttk.Frame(frame_left)
		label = ttk.Label(
			master 				= frame,
			text 				= "비밀번호",
			justify 			= tkinter.LEFT,
			width 				= 10
			)
		label.pack(side=tkinter.LEFT)

		self.option_dic[lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT + '_passwd'] = tkinter.StringVar(frame)
		if not lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT + '_passwd' in self.configure.common_config:
			self.configure.common_config[lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT + '_passwd'] = ''
		self.option_dic[lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT + '_passwd'].set(self.configure.common_config[lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT + '_passwd'])

		entry = ttk.Entry(
			master 				= frame,
			textvariable 		= self.option_dic[lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT + '_passwd'],
			show 				= "*",
			width 				= 24
			)
		entry.pack(side=tkinter.LEFT, padx=2)
		frame.pack()
		frame_left.pack(anchor=tkinter.W, side=tkinter.LEFT)

		frame_extra = ttk.Frame(frame_top)
		frame_extra.pack(side=tkinter.LEFT, padx=1)

		frame_right = ttk.Frame(frame_top)

		button = ttk.Button(
			master 				= frame_right, 
			text 				= "로그인",
			command 			= lambda : self.callback_login_button(None)
			)
		button.pack(fill=tkinter.BOTH, expand=True)
		frame_right.pack(fill=tkinter.BOTH, expand=True)
		frame_top.pack()

		if not lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT in self.configure.common_config:
			self.configure.common_config[lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT] = False

		self.option_dic[lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT] = tkinter.BooleanVar()
		self.option_dic[lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT].trace(
			'w', lambda *args: self.callback_save_login_account_booleanvar(args, lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT)
			)
		self.option_dic[lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT].set(self.configure.common_config[lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT])

		frame_bottom = ttk.Frame(label_frame)
		frame = ttk.Frame(frame_bottom)
		check_box = ttk.Checkbutton(

			master 				= frame_bottom,
			text 				= '아이디/비밀번호 기억하기', 
			variable 			= self.option_dic[lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT],
			onvalue 			= True, 
			offvalue 			= False
			)

		check_box.pack(anchor=tkinter.E)
		frame.pack(anchor=tkinter.E)
		frame_extra = ttk.Frame(frame_bottom)
		frame_extra.pack(pady=1)
		s = ttk.Style()
		s.configure('label_link.TLabel', foreground='blue', font=('굴림체', 9, 'underline'))
		link_url = "회원가입"
		frame = ttk.Frame(frame_bottom)
		label = ttk.Label(
			master 				= frame, 
			text 				= link_url,
			justify 			= tkinter.LEFT,
			style 				= 'label_link.TLabel',
			cursor 				= 'hand2'
			)
		label.pack(side=tkinter.LEFT)
		label.bind("<Button-1>", self.callback_hompage)
		frame.pack(anchor=tkinter.E)
		frame_bottom.pack(fill=tkinter.X, pady=5)
		label_frame.pack(padx=5, pady=10)
		self.main_frame.pack(fill=tkinter.BOTH, expand=True)

		self.master.bind('<Return>', self.callback_login_button)

	def callback_hompage(self, event):
		webbrowser.open_new(r"https://www.dogfooter.com")

	def callback_login_button(self, event):
		self.callback_save_login_account_booleanvar(None, lybconstant.LYB_DO_BOOLEAN_SAVE_LOGIN_ACCOUNT)
		self.main_frame.pack_forget()
		likeyoubot_gui.LYBGUI(self.master, self.configure)

	def callback_save_login_account_booleanvar(self, args, option_name):
		self.configure.common_config[option_name] = self.option_dic[option_name].get()
		if self.configure.common_config[option_name] != True:
			self.configure.common_config[option_name + '_id'] = ''
			self.configure.common_config[option_name + '_passwd'] = ''
		else:
			self.configure.common_config[option_name + '_id'] = self.option_dic[option_name + '_id'].get()
			self.configure.common_config[option_name + '_passwd'] = self.option_dic[option_name + '_passwd'].get()
			print(self.option_dic[option_name + '_passwd'].get() )

		try:
			with open(self.configure.path, 'wb') as dat_file:
				pickle.dump(self.configure, dat_file)
		except:
			print('[DEBUG] Exception: ' + str(sys.exc_info()[0]) + '(' +str(sys.exc_info()[1]) + ')')
		

	# 	frame.pack()

	# 	s = ttk.Style()
	# 	s.configure('label_link.TLabel', foreground='blue', font=('굴림체', 9, 'underline'))
	# 	link_url = "www.dogfooter.com"

	# 	label_hompage = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= link_url,
	# 		justify 			= tkinter.LEFT,
	# 		style 				= 'label_link.TLabel',
	# 		cursor 				= 'hand2'
	# 		# fg='White' if brightness < 120 else 'Black', 
	# 		# bg=bg_colour
	# 		)

	# 	# f = font.Font(label_hompage, label_hompage.cget("font"))
	# 	# f.configure(underline = True)
	# 	# f.configure(weight='bold')
	# 	# label_hompage.configure(font=f)

	# 	label_hompage.pack(side=tkinter.LEFT)
	# 	label_hompage.bind("<Button-1>", self.callback_hompage)

	# 	frame.pack(anchor=tkinter.W, fill=tkinter.BOTH)

	# 	frame = ttk.Frame(self.tab_frame[-1], relief=frame_relief)
	# 	label_begging = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= "※ 소스가 궁금하신 분들은 오른쪽 링크를 클릭하세요                 → ",
	# 		justify 			= tkinter.LEFT
	# 		)
	# 	label_begging.pack(side=tkinter.LEFT)

	# 	link_url = "www.bitbucket.org/dogfooter/dogfooter"

	# 	label_hompage = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= link_url,
	# 		justify 			= tkinter.LEFT, 
	# 		style 				= 'label_link.TLabel',
	# 		cursor 				= 'hand2'
	# 		)

	# 	# f = font.Font(label_hompage, label_hompage.cget("font"))
	# 	# f.configure(underline = True)
	# 	# f.configure(weight='bold')
	# 	# label_hompage.configure(font=f)

	# 	label_hompage.pack(side=tkinter.LEFT)
	# 	label_hompage.bind("<Button-1>", self.callbac_bitbucket)

	# 	frame.pack(anchor=tkinter.W, fill=tkinter.BOTH, pady=5)

	# 	frame = ttk.Frame(self.tab_frame[-1], relief=frame_relief)
	# 	self.keyword_label = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= "앱 플레이어 창 이름",
	# 		justify 			= tkinter.LEFT, 
	# 		# font				= lybconstant.LYB_FONT
	# 		# fg='White' if brightness < 120 else 'Black', 
	# 		# bg=bg_colour
	# 		)
	# 	self.keyword_label.pack(side=tkinter.LEFT)

	# 	self.keyword_entry = ttk.Entry(
	# 		master 				= frame, 
	# 		# relief 				= 'sunken', 
	# 		justify 			= tkinter.LEFT, 
	# 		# font				= lybconstant.LYB_FONT, 
	# 		width 				= 32
	# 		)
	# 	self.keyword_entry.pack(side=tkinter.LEFT, padx=10)
	# 	self.keyword_entry.insert(0, self.configure.keyword)
	# 	self.keyword_entry.focus()

	# 	s = ttk.Style()
	# 	s.configure('button_0.TButton')

	# 	self.search_button = ttk.Button(
	# 		master 				= frame, 
	# 		text 				= "검색", 
	# 		style 				= 'button_0.TButton', 
	# 		width 				= 10,
	# 		# font				= lybconstant.LYB_FONT,
	# 		command 			= lambda: self.searchWindow(None)
	# 		)
	# 	self.search_button.pack(side=tkinter.LEFT)


	# 	# self.security_authority = False

	# 	# self.keyword_label = ttk.Label(
	# 	# 	master 				= frame, 
	# 	# 	text 				= "실행 인증 코드",
	# 	# 	justify 			= tkinter.LEFT, 
	# 	# 	font				= lybconstant.LYB_FONT
	# 	# 	)
	# 	# self.keyword_label.pack(side=tkinter.LEFT, padx=10)

	# 	# self.security_code = tkinter.StringVar(frame)
	# 	# security_code_entry = tkinter.Entry(
	# 	# 	master 				= frame, 
	# 	# 	relief 				= 'sunken', 
	# 	# 	justify 			= tkinter.LEFT, 
	# 	# 	font				= lybconstant.LYB_FONT,
	# 	# 	textvariable 		= self.security_code,
	# 	# 	width 				= 32
	# 	# 	)

	# 	# self.security_code.trace( 
	# 	# 	'w', lambda *args: self.callback_security_code_stringvar(args)
	# 	# 	)
	# 	# security_code_entry.pack(side=tkinter.LEFT)
	# 	# if not 'security_code' in self.configure.common_config:
	# 	# 	self.configure.common_config['security_code'] = ''
	# 	# security_code_entry.insert(0, self.configure.common_config['security_code'])

	# 	frame.pack(side=tkinter.TOP, pady=5)

	# 	frame_s = ttk.Frame(
	# 		master 				= self.tab_frame[-1], 
	# 		relief 				= frame_relief
	# 		)
	# 	frame_l = ttk.Frame(frame_s, relief=frame_relief)


	# 	s = ttk.Style()
	# 	s.configure('label_1.TLabel', font=('굴림체', 9, 'underline'))

	# 	self.configure_label = ttk.Label(
	# 		master				= frame_l,
	# 		text 				= lybconstant.LYB_LABEL_SELECT_WINDOW_TEXT,
	# 		style 				= 'label_1.TLabel'
	# 		)
	# 	self.configure_label.pack(side=tkinter.TOP)

	# 	# label_font = tkinter.font.Font(self.configure_label, self.configure_label.cget('font'))
	# 	# label_font.configure(underline=True)
	# 	# self.configure_label.configure(font=label_font)

	# 	self.gui_config_dic = {}

	# 	self.games = [ 
	# 		lybconstant.LYB_GAME_TERA,
	# 		# lybconstant.LYB_GAME_LIN2REV,
	# 		# lybconstant.LYB_GAME_CLANS,
	# 		# lybconstant.LYB_GAME_YEOLHYUL
	# 		]

	# 	self.gui_config_dic['games'] = tkinter.StringVar(frame_l)
	# 	if not 'games' in self.configure.common_config:
	# 		self.configure.common_config['games'] = self.games[0]

	# 	self.gui_config_dic['games'].set(self.configure.common_config['games'])
	# 	self.gui_config_dic['games'].trace('w',
	# 		lambda *args: self.selected_game(args))

	# 	combobox = ttk.Combobox(
	# 		master 			= frame_l,
	# 		values			= self.games,
	# 		textvariable	= self.gui_config_dic['games'],
	# 		state 			= 'readonly',
	# 		width 			= 25,
	# 		font 			= lybconstant.LYB_FONT
	# 		)
	# 	# self.inactive_flag_option_menu.set(inactive_mode_flag_list[0])
	# 	# combobox.configure(stat=tkinter.DISABLED)
	# 	combobox.pack(side=tkinter.TOP)


	# 	# option_menu = ttk.OptionMenu(
	# 	# 	frame_l,
	# 	# 	self.gui_config_dic['games'], 
	# 	# 	'',
	# 	# 	*self.games, 
	# 	# 	command 			= self.selected_game

	# 	# 	)
	# 	# option_menu.configure(width=20)
	# 	# # option_menu.configure(font=lybconstant.LYB_FONT)
	# 	# option_menu.pack(side=tkinter.TOP)

	# 	if not 'multi_account' in self.configure.common_config:
	# 		self.configure.common_config['multi_account'] = False

	# 	# 로컬변수로 선언하면 안된다. 가비지컬렉터한테 먹혀서 안됨.. UI 는 계속 루프를 도니까
	# 	self.gui_config_dic['multi_account'] = tkinter.BooleanVar()
	# 	self.gui_config_dic['multi_account'].set(self.configure.common_config['multi_account'])

	# 	check_box = ttk.Checkbutton(

	# 		master 				= frame_l,
	# 		text 				= '구글 멀티 계정 사용', 
	# 		variable 			= self.gui_config_dic['multi_account'],
	# 		onvalue 			= True, 
	# 		offvalue 			= False,
	# 		command 			= lambda: self.toggleCommonCheckBox('multi_account')

	# 		)

	# 	check_box.pack(anchor=tkinter.W)


	# 	if not 'debug_booleanvar' in self.configure.common_config:
	# 		self.configure.common_config['debug_booleanvar'] = True

	# 	self.gui_config_dic['debug_booleanvar'] = tkinter.BooleanVar()
	# 	self.gui_config_dic['debug_booleanvar'].set(self.configure.common_config['debug_booleanvar'])

	# 	check_box = ttk.Checkbutton(

	# 		master 				= frame_l,
	# 		text 				= '디버깅 모드', 
	# 		variable 			= self.gui_config_dic['debug_booleanvar'],
	# 		onvalue 			= True, 
	# 		offvalue 			= False,
	# 		command 			= lambda: self.toggle_debug_checkbox('debug_booleanvar')

	# 		)

	# 	check_box.pack(anchor=tkinter.W)




	# 	frame = ttk.Frame(frame_l, relief=frame_relief)
	# 	if not lybconstant.LYB_DO_BOOLEAN_USE_INACTIVE_MODE in self.configure.common_config:
	# 		self.configure.common_config[lybconstant.LYB_DO_BOOLEAN_USE_INACTIVE_MODE] = False

	# 	self.gui_config_dic[lybconstant.LYB_DO_BOOLEAN_USE_INACTIVE_MODE] = tkinter.BooleanVar()
	# 	self.gui_config_dic[lybconstant.LYB_DO_BOOLEAN_USE_INACTIVE_MODE].set(self.configure.common_config[lybconstant.LYB_DO_BOOLEAN_USE_INACTIVE_MODE])
				
	# 	check_box = ttk.Checkbutton(

	# 		master 				= frame,
	# 		text 				= '비활성 모드', 
	# 		variable 			= self.gui_config_dic[lybconstant.LYB_DO_BOOLEAN_USE_INACTIVE_MODE],
	# 		onvalue 			= True, 
	# 		offvalue 			= False,
	# 		command 			= lambda: self.callback_use_inactive_mode_booleanvar()

	# 		)

	# 	check_box.pack(side=tkinter.LEFT)

	# 	inactive_mode_flag_list = [ 
	# 		'윈7',
	# 		'윈10'
	# 		]

	# 	self.gui_config_dic[lybconstant.LYB_DO_STRING_INACTIVE_MODE_FLAG] = tkinter.StringVar(frame)
	# 	if not lybconstant.LYB_DO_STRING_INACTIVE_MODE_FLAG in self.configure.common_config:
	# 		self.configure.common_config[lybconstant.LYB_DO_STRING_INACTIVE_MODE_FLAG] = inactive_mode_flag_list[1]
	# 	self.gui_config_dic[lybconstant.LYB_DO_STRING_INACTIVE_MODE_FLAG].set(self.configure.common_config[lybconstant.LYB_DO_STRING_INACTIVE_MODE_FLAG])
	# 	self.gui_config_dic[lybconstant.LYB_DO_STRING_INACTIVE_MODE_FLAG].trace('w',
	# 		lambda *args: self.callback_inactive_mode_flag_stringvar(args))
	# 	# self.inactive_flag_option_menu = ttk.OptionMenu(
	# 	# 	frame,
	# 	# 	self.gui_config_dic[lybconstant.LYB_DO_STRING_INACTIVE_MODE_FLAG], 
	# 	# 	'',
	# 	# 	*inactive_mode_flag_list, 
	# 	# 	command 			= self.callback_inactive_mode_flag_stringvar

	# 	# 	)
	# 	# self.inactive_flag_option_menu.configure(width=4)
	# 	# self.inactive_flag_option_menu.configure(stat=tkinter.DISABLED)
	# 	# # self.inactive_flag_option_menu.configure(font=lybconstant.LYB_FONT)
	# 	# self.inactive_flag_option_menu.pack(side=tkinter.LEFT, anchor=tkinter.SW)
	# 	s = ttk.Style()
	# 	s.map('TCombobox', fieldbackground=[('disabled', '#afafaf')])
	# 	s.map('TCombobox', foreground=[('disabled', '#424242')])

	# 	self.inactive_flag_option_menu = ttk.Combobox(
	# 		master 			= frame,
	# 		values			= inactive_mode_flag_list,
	# 		textvariable	= self.gui_config_dic[lybconstant.LYB_DO_STRING_INACTIVE_MODE_FLAG],
	# 		state 			= 'readonly',
	# 		width 			= 5,
	# 		font 			= lybconstant.LYB_FONT
	# 		)
	# 	# self.inactive_flag_option_menu.set(inactive_mode_flag_list[0])
	# 	self.inactive_flag_option_menu.configure(stat=tkinter.DISABLED)
	# 	self.inactive_flag_option_menu.pack(anchor=tkinter.W, side=tkinter.LEFT)


	# 	frame.pack(anchor=tkinter.W)

	# 	frame_l.pack(side=tkinter.LEFT, anchor=tkinter.NW, padx=2)

	# 	frame_c = ttk.Frame(frame_s, relief=frame_relief)
	# 	# ----- WINDOW LIST -----
	# 	self.search_window = tkinter.Listbox(

	# 		master				= frame_c, 
	# 		selectmode			= tkinter.MULTIPLE, 
	# 		font				= ("돋움", 10),
	# 		height 				= 8,
	# 		activestyle			= 'none',
	# 		selectbackground	= "#BC80CC"

	# 		)
	# 	self.search_window.pack(side=tkinter.LEFT, anchor=tkinter.NW)
	# 	self.selected_window_list = []
	# 	#self.search_window.insert('end', '')
	# 	self.search_window.bind('<<ListboxSelect>>', self.selectedWindowList)
	# 	self.is_clicked_common_tab = False
	# 	frame_c.pack(side=tkinter.LEFT, anchor=tkinter.NW, padx=10)

	# 	frame_r = ttk.Frame(frame_s, relief=frame_relief)
	# 	label = ttk.Label(
	# 		master 				= frame_r,
	# 		text 				= '※ 필독 사항 및 사용법 ※', 
	# 		style 				= 'label_0.TLabel',
	# 		justify 			= tkinter.LEFT
	# 		)
	# 	label.pack(anchor=tkinter.NW, pady=2)
	# 	# label_font = tkinter.font.Font(label, label.cget('font'))
	# 	# label_font.configure(underline=True)
	# 	# label_font.configure(weight='bold')
	# 	# label.configure(font=label_font)
	# 	usage_text = tkinter.Text(
	# 		master 				= frame_r, 
	# 		spacing1 			= 3,
	# 		height 				= 6,
	# 		font 				= lybconstant.LYB_FONT
	# 		)

	# 	# vsb = tkinter.Scrollbar(
	# 	# 	master 				= usage_text,
	# 	# 	orient 				= 'vertical',
	# 	# 	command 			= usage_text.yview
	# 	# 	)
	# 	# usage_text.configure(yscrollcommand=vsb.set)
	# 	# vsb.pack(side='right', fill='y')

	# 	# hsb = tkinter.Scrollbar(
	# 	# 	master 				= usage_text,
	# 	# 	orient				= 'horizontal',
	# 	# 	command 			= usage_text.xview
	# 	# 	)
	# 	# usage_text.configure(xscrollcommand=hsb.set)
	# 	# hsb.pack(side='bottom', fill='x')

	# 	usage_text.pack(anchor=tkinter.NW, fill=tkinter.X, expand=True)

	# 	usage_list = lybconstant.LYB_USAGE.split('\n')
	# 	for each_usage in usage_list:
	# 		usage_text.insert('end', each_usage + '\n')

	# 	frame_r.pack(side=tkinter.LEFT, anchor=tkinter.NW, fill=tkinter.X, expand=True, padx=5)
	# 	frame_s.pack(anchor=tkinter.NW, fill=tkinter.X, pady=10)





	# 	# - TAB












	# 	# 탭 추가
	# 	self.option_dic['common_tab'] = ttk.Notebook(
	# 		master 				= self.tab_frame[-1]
	# 		)
		
	# 	# 모니터링 탭
	# 	self.option_dic['monitoring_tab'] = ttk.Frame(
	# 		master 				= self.option_dic['common_tab']
	# 		)
		
	# 	# self.gui_config_dic['monitoring_tab'].pack(anchor=tkinter.NW, fill=tkinter.BOTH, expand=True)
	# 	self.option_dic['common_tab'].add(self.option_dic['monitoring_tab'], text='모니터링')

	# 	# 공통 설정 탭
	# 	self.option_dic['common_config_tab'] = ttk.Frame(
	# 		master 				= self.option_dic['common_tab']
	# 		)
		
	# 	# self.gui_config_dic['common_config_tab'].pack(anchor=tkinter.NW, fill=tkinter.BOTH, expand=True)
	# 	self.option_dic['common_tab'].add(self.option_dic['common_config_tab'], text='공통 설정')


	# 	self.option_dic['common_tab'].pack(anchor=tkinter.NW, fill=tkinter.BOTH, expand=True)





	# 	self.option_dic['monitor_master'] = ttk.Frame(self.option_dic['monitoring_tab'])


	# 	frame_label = self.add_monitor_master_frame()


	# 	self.option_dic['monitor_master'].pack(anchor=tkinter.NW)

	# 	frame_bottom = ttk.Frame(self.option_dic['monitoring_tab'])
	# 	# ----- INFORMATION LOGGING ------
	# 	self.information_frame = ttk.Frame(frame_bottom)

	# 	self.information = tkinter.Text(
	# 		master 				= self.information_frame,  
	# 		width 				= 114, 
	# 		height 				= 10,
	# 		spacing1 			= 3,
	# 		font 				= lybconstant.LYB_FONT
	# 		)
	# 	# self.vsb = tkinter.Scrollbar(self.information_frame,
	# 	# 	orient='vertical',
	# 	# 	command=self.information.yview)
	# 	# self.hsb = tkinter.Scrollbar(self.information_frame,
	# 	# 	orient='horizontal',
	# 	# 	command=self.information.xview)
	# 	self.information.configure(
	# 		wrap=tkinter.NONE)
	# 		# yscrollcommand=self.vsb.set, 
	# 		# xscrollcommand=self.hsb.set)
	# 	self.information.tag_configure('FAIL', foreground='red')
	# 	self.information.tag_configure('SUCCESS', foreground='green')
	# 	self.information.tag_configure('GOOD', foreground='#ad42f4')
	# 	self.information.tag_configure('NICE', foreground='#00ad56')
	# 	self.information.tag_configure('SUB', foreground='#d13e83')
	# 	self.information.tag_configure('BAD', foreground='#fcab97')
	# 	# self.information.tag_configure('GOOD', foreground='#ad42f4')
	# 	self.information.tag_configure('INFO', foreground='blue')
	# 	# self.vsb.pack(side='right', fill='y')
	# 	# self.hsb.pack(side='bottom', fill='x')
	# 	self.information.pack(side=tkinter.BOTTOM)
	# 	self.information_frame.pack(side=tkinter.LEFT, anchor=tkinter.SW, pady=5, padx=5)

	# 	# ----- BUTTON FRAME -----
	# 	filter_frame = ttk.Frame(frame_bottom)

	# 	self.log_filter_entry = tkinter.StringVar(frame)
	# 	self.log_filter_entry.trace( 'w', lambda *args: self.callback_log_filter_entry_stringvar(args) )

	# 	self.configure.common_config[lybconstant.LYB_DO_STRING_LOG_FILTER] = ''
		
	# 	entry = ttk.Entry(
	# 		master 				= filter_frame,
	# 		textvariable 		= self.log_filter_entry,
	# 		justify 			= tkinter.LEFT, 
	# 		width 				= 15
	# 		)

	# 	entry.pack(side=tkinter.LEFT)
	# 	filter_frame.pack(anchor=tkinter.NW, side=tkinter.TOP, pady=5)

	# 	s = ttk.Style()
	# 	s.configure('button_1.TButton', justify=tkinter.CENTER)
	# 	button_frame = ttk.Frame(frame_bottom)
	# 	self.start_button = ttk.Button(
	# 		master 				= button_frame, 
	# 		text 				= "시작\n[F1]", 
	# 		width 				= 14,
	# 		style 				= 'button_1.TButton',
	# 		command 			= lambda:self.startWorker(None)
	# 		)	


	# 	self.pause_button = ttk.Button(
	# 		master 				= button_frame, 
	# 		text 				= "일시정지\n[F2]",  
	# 		width 				= 14,
	# 		style 				= 'button_1.TButton',
	# 		command 			= lambda: self.pauseWorker(None)
	# 		)		


	# 	self.stop_button = ttk.Button(
	# 		master 				= button_frame, 
	# 		text 				= "정지\n[F3]",  
	# 		width 				= 14,
	# 		style 				= 'button_1.TButton',
	# 		command 			= lambda: self.terminateWorker(None)
	# 		)


	# 	self.start_button.pack(pady=lybconstant.LYB_PADDING)
	# 	self.pause_button.pack(pady=lybconstant.LYB_PADDING)
	# 	self.stop_button.pack(pady=lybconstant.LYB_PADDING)

	# 	button_frame.pack(side=tkinter.BOTTOM, anchor=tkinter.W)

	# 	frame_bottom.pack(side=tkinter.BOTTOM)









	# 	# ----- CONFIGURATION -----
	# 	self.configure_frame = ttk.LabelFrame(
	# 		master 				= self.option_dic['common_config_tab'], 
	# 		text 				= '봇 설정'
	# 		)

	# 	frame = ttk.Frame(self.configure_frame, relief=frame_relief)

	# 	threshold_label = ttk.Label(
	# 		master 				= frame,
	# 		text 				= "이미지를 인식할 때 비교 대상과 ",
	# 		anchor 				= tkinter.W,
	# 		justify 			= tkinter.LEFT
	# 		# fg='White' if brightness < 120 else 'Black', 
	# 		# bg=bg_colour
	# 		)

		
	# 	# countif.place(
	# 	# 	x=lybconstant.LYB_PADDING,
	# 	# 	y=lybconstant.LYB_PADDING,
	# 	# 	width=lybconstant.LYB_LABEL_WIDTH, height=lybconstant.LYB_LABEL_HEIGHT
	# 	# 	)
		

	# 	threshold_label.pack(side=tkinter.LEFT)


	# 	self.threshold_entry = tkinter.StringVar(frame)
	# 	entry = ttk.Entry(
	# 		master 				= frame, 
	# 		justify 			= tkinter.RIGHT,  
	# 		textvariable 		= self.threshold_entry,
	# 		width 				= 3
	# 		)
	# 	entry.pack(side=tkinter.LEFT)

	# 	if not 'threshold_entry' in self.configure.common_config:
	# 		self.configure.common_config['threshold_entry'] = 0.7

	# 	self.threshold_entry.set(str(int(self.configure.common_config['threshold_entry'] * 100)))
	# 	self.threshold_entry.trace('w', lambda *args: self.callback_threshold_entry(args))

	# 	label = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= "% 이상 동일하면 감지하도록 설정합니다", 
	# 		justify 			= tkinter.LEFT
	# 		# fg='White' if brightness < 120 else 'Black', 
	# 		# bg=bg_colour
	# 		)
	# 	label.pack(side=tkinter.LEFT)

	# 	frame.pack(anchor=tkinter.W)

	# 	frame = ttk.Frame(self.configure_frame, relief=frame_relief)
	# 	label = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= "이미지를 인식할 때 RGB 값의 차이가 ",
	# 		anchor 				= tkinter.W,
	# 		justify 			= tkinter.LEFT
	# 		)
	# 	label.pack(side=tkinter.LEFT)
		
	# 	self.pixel_tolerance_entry = tkinter.StringVar(frame)
	# 	entry = ttk.Entry(
	# 		master 				= frame,  
	# 		justify 			= tkinter.RIGHT, 
	# 		textvariable		= self.pixel_tolerance_entry,
	# 		width 				= 3
	# 		)
	# 	entry.pack(side=tkinter.LEFT)	

	# 	if not 'pixel_tolerance_entry' in self.configure.common_config:
	# 		self.configure.common_config['pixel_tolerance_entry'] = 30

	# 	self.pixel_tolerance_entry.set(str(int(self.configure.common_config['pixel_tolerance_entry'])))
	# 	self.pixel_tolerance_entry.trace('w', lambda *args: self.callback_pixel_tolerance_entry(args))

	# 	label = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= "이하는 같은 이미지로 간주합니다.",
	# 		justify 			= tkinter.LEFT
	# 		# fg='White' if brightness < 120 else 'Black', 
	# 		# bg=bg_colour
	# 		)
	# 	label.pack(side=tkinter.LEFT)

	# 	frame.pack(anchor=tkinter.W)

	# 	frame = ttk.Frame(self.configure_frame, relief=frame_relief)
	# 	label = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= "이미지 인식이 안 될 경우 찾을 때까지 지속적으로", 
	# 		anchor 				= tkinter.W,
	# 		justify 			= tkinter.LEFT
	# 		)
	# 	label.pack(side=tkinter.LEFT)

	# 	self.adjust_entry = tkinter.StringVar(frame)
	# 	entry = ttk.Entry(
	# 		master 				= frame, 
	# 		justify 			= tkinter.RIGHT, 
	# 		textvariable 		= self.adjust_entry,
	# 		width 				= 3
	# 		)
	# 	entry.pack(side=tkinter.LEFT)

	# 	if not 'adjust_entry' in self.configure.common_config:
	# 		self.configure.common_config['adjust_entry'] = 10

	# 	self.adjust_entry.set(str(int(self.configure.common_config['adjust_entry'])))
	# 	self.adjust_entry.trace('w', lambda *args: self.callback_adjust_entry(args))

	# 	label = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= "% 씩 가중치를 줍니다", 
	# 		justify 			= tkinter.LEFT
	# 		# fg='White' if brightness < 120 else 'Black', 
	# 		# bg=bg_colour
	# 		)
	# 	label.pack(side=tkinter.LEFT)
	# 	frame.pack(anchor=tkinter.W)

	# 	frame = ttk.Frame(self.configure_frame, relief=frame_relief)
	# 	label = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= "봇의 작업 주기를 ", 
	# 		anchor 				= tkinter.W,
	# 		justify 			= tkinter.LEFT
	# 		# fg='White' if brightness < 120 else 'Black', 
	# 		# bg=bg_colour
	# 		)

		
	# 	# countif.place(
	# 	# 	x=lybconstant.LYB_PADDING,
	# 	# 	y=lybconstant.LYB_PADDING,
	# 	# 	width=lybconstant.LYB_LABEL_WIDTH, height=lybconstant.LYB_LABEL_HEIGHT
	# 	# 	)
	# 	label.pack(side=tkinter.LEFT)
		
	# 	self.wakeup_period_entry = tkinter.StringVar(frame)
	# 	entry = ttk.Entry(
	# 		master 				= frame, 
	# 		justify 			= tkinter.RIGHT,
	# 		textvariable 		= self.wakeup_period_entry,
	# 		width 				= 6
	# 		)
	# 	entry.pack(side=tkinter.LEFT)		
	# 	label = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= "초로 설정합니다", 
	# 		justify 			= tkinter.LEFT
	# 		# fg='White' if brightness < 120 else 'Black', 
	# 		# bg=bg_colour
	# 		)
	# 	label.pack(side=tkinter.LEFT)

	# 	if not 'wakeup_period_entry' in self.configure.common_config:
	# 		self.configure.common_config['wakeup_period_entry'] = float(1.0)

	# 	frame.pack(anchor=tkinter.W)
	# 	self.wakeup_period_entry.set(str(self.configure.common_config['wakeup_period_entry']))
	# 	self.wakeup_period_entry.trace('w', lambda *args: self.callback_wakeup_period_entry(args))








	# 	frame = ttk.Frame(self.configure_frame, relief=frame_relief)
	# 	label = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= "UI 갱신 주기를 ", 
	# 		anchor 				= tkinter.W,
	# 		justify 			= tkinter.LEFT
	# 		)

	# 	label.pack(side=tkinter.LEFT)
		
	# 	self.update_period_ui_entry = tkinter.StringVar(frame)
	# 	entry = ttk.Entry(
	# 		master 				= frame, 
	# 		justify 			= tkinter.RIGHT,
	# 		textvariable 		= self.update_period_ui_entry,
	# 		width 				= 6
	# 		)
	# 	entry.pack(side=tkinter.LEFT)		
	# 	label = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= "초로 설정합니다", 
	# 		justify 			= tkinter.LEFT
	# 		)
	# 	label.pack(side=tkinter.LEFT)

	# 	if not lybconstant.LYB_DO_STRING_PERIOD_UPDATE_UI in self.configure.common_config:
	# 		self.configure.common_config[lybconstant.LYB_DO_STRING_PERIOD_UPDATE_UI] = float(1)

	# 	frame.pack(anchor=tkinter.W)
	# 	self.update_period_ui_entry.set(str(self.configure.common_config[lybconstant.LYB_DO_STRING_PERIOD_UPDATE_UI]))
	# 	self.update_period_ui_entry.trace('w', lambda *args: self.callback_update_period_ui_entry(args))





	# 	frame = ttk.Frame(self.configure_frame, relief=frame_relief)		
	# 	self.use_monitoring_flag = tkinter.BooleanVar(frame)

	# 	label = ttk.Checkbutton(
	# 		master 				= frame, 
	# 		text 				= "모니터링 기능을 사용합니다", 
	# 		variable 			= self.use_monitoring_flag,
	# 		onvalue 			= True, 
	# 		offvalue 			= False
	# 		)

	# 	label.pack(side=tkinter.LEFT)

	# 	if not lybconstant.LYB_DO_BOOLEAN_USE_MONITORING in self.configure.common_config:
	# 		self.configure.common_config[lybconstant.LYB_DO_BOOLEAN_USE_MONITORING] = True

	# 	frame.pack(anchor=tkinter.W)
	# 	self.use_monitoring_flag.set(
	# 		self.configure.common_config[lybconstant.LYB_DO_BOOLEAN_USE_MONITORING]
	# 		)
	# 	self.use_monitoring_flag.trace('w', 
	# 		lambda *args: self.callback_use_monitoring_booleanvar(args)
	# 		)


	# 	frame = ttk.Frame(self.configure_frame, relief=frame_relief)

	# 	label = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= "게임 화면 전환 후", 
	# 		anchor 				= tkinter.W,
	# 		justify 			= tkinter.LEFT
	# 		)
	# 	label.pack(side=tkinter.LEFT)
	# 	self.wait_time_scene_change = tkinter.StringVar(frame)
	# 	entry = ttk.Entry(
	# 		master 				= frame, 
	# 		justify 			= tkinter.RIGHT, 
	# 		textvariable 		= self.wait_time_scene_change,
	# 		width 				= 3
	# 		)
	# 	entry.pack(side=tkinter.LEFT)		
	# 	label = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= "초 동안 대기합니다", 
	# 		justify 			= tkinter.LEFT
	# 		# fg='White' if brightness < 120 else 'Black', 
	# 		# bg=bg_colour
	# 		)
	# 	label.pack(side=tkinter.LEFT)

	# 	if not lybconstant.LYB_DO_STRING_WAIT_TIME_SCENE_CHANGE in self.configure.common_config:
	# 		self.configure.common_config[lybconstant.LYB_DO_STRING_WAIT_TIME_SCENE_CHANGE] = 0

	# 	frame.pack(anchor=tkinter.W)
	# 	self.wait_time_scene_change.set(self.configure.common_config[lybconstant.LYB_DO_STRING_WAIT_TIME_SCENE_CHANGE])
	# 	self.wait_time_scene_change.trace('w', lambda *args: self.callback_wait_time_scene_change(args))



	# 	frame = ttk.Frame(self.configure_frame, relief=frame_relief)
	# 	label = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= "매크로 실행 중 에러가 발생하면 최대", 
	# 		anchor 				= tkinter.W,
	# 		justify 			= tkinter.LEFT
	# 		)
	# 	label.pack(side=tkinter.LEFT)

	# 	self.recovery_count_stringvar = tkinter.StringVar(frame)
	# 	entry = ttk.Entry(
	# 		master 				= frame, 
	# 		justify 			= tkinter.RIGHT, 
	# 		textvariable 		= self.recovery_count_stringvar,
	# 		width 				= 3
	# 		)
	# 	entry.pack(side=tkinter.LEFT)		
	# 	label = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= "회 재실행 시킵니다", 
	# 		justify 			= tkinter.LEFT
	# 		# fg='White' if brightness < 120 else 'Black', 
	# 		# bg=bg_colour
	# 		)
	# 	label.pack(side=tkinter.LEFT)

	# 	if not lybconstant.LYB_DO_STRING_RECOVERY_COUNT in self.configure.common_config:
	# 		self.configure.common_config[lybconstant.LYB_DO_STRING_RECOVERY_COUNT] = 5

	# 	frame.pack(anchor=tkinter.W)
	# 	self.recovery_count_stringvar.set(self.configure.common_config[lybconstant.LYB_DO_STRING_RECOVERY_COUNT])
	# 	self.recovery_count_stringvar.trace('w', lambda *args: self.callback_recovery_count_stringvar(args))




	# 	frame = ttk.Frame(self.configure_frame, relief=frame_relief)
	# 	label = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= "APP 종료 행동을 ", 
	# 		anchor 				= tkinter.W,
	# 		justify 			= tkinter.LEFT
	# 		)
	# 	label.pack(side=tkinter.LEFT)

	# 	self.close_app_stringvar = tkinter.StringVar(frame)
	# 	entry = ttk.Entry(
	# 		master 				= frame, 
	# 		justify 			= tkinter.RIGHT, 
	# 		textvariable 		= self.close_app_stringvar,
	# 		width 				= 3
	# 		)
	# 	entry.pack(side=tkinter.LEFT)		
	# 	label = ttk.Label(
	# 		master 				= frame, 
	# 		text 				= "회 실행 시킵니다", 
	# 		justify 			= tkinter.LEFT
	# 		# fg='White' if brightness < 120 else 'Black', 
	# 		# bg=bg_colour
	# 		)
	# 	label.pack(side=tkinter.LEFT)

	# 	if not lybconstant.LYB_DO_STRING_CLOSE_APP_COUNT in self.configure.common_config:
	# 		self.configure.common_config[lybconstant.LYB_DO_STRING_CLOSE_APP_COUNT] = 5

	# 	frame.pack(anchor=tkinter.W)
	# 	self.close_app_stringvar.set(self.configure.common_config[lybconstant.LYB_DO_STRING_CLOSE_APP_COUNT])
	# 	self.close_app_stringvar.trace('w', lambda *args: self.callback_close_app_stringvar(args))



	# 	# self.threshold_entry.place(
	# 	# 	x=lybconstant.LYB_LABEL_WIDTH + 5*lybconstant.LYB_PADDING,
	# 	# 	y=lybconstant.LYB_PADDING,
	# 	# 	width=2*lybconstant.LYB_LABEL_WIDTH - 8*lybconstant.LYB_PADDING, 
	# 	# 	height=lybconstant.LYB_LABEL_HEIGHT
	# 	# 	)
	# 	# self.keyword_entry.insert(0, self.configure.keyword)

	# 	self.configure_frame.pack(anchor=tkinter.NW, fill=tkinter.X, expand=True, pady=10)















	# 	for i in range(len(self.games)):
	# 		self.game_options[self.games[i]] = {}
	# 		self.game_frame[self.games[i]] = {}
	# 		self.tab_frame.append(ttk.Frame(self.note,
	# 			width=self.width - lybconstant.LYB_PADDING,
	# 			height=self.height - lybconstant.LYB_PADDING,
	# 			relief='groove'
	# 			))
	# 		self.note.add(self.tab_frame[i+1], text=self.games[i])

	# 	# self.configure.common_config[self.games[0]] = {}
	# 	# self.configure.common_config[self.games[0]]['work_list'] = []

	# 	# for each_work in LYBLIN2REV.LYBLineage2Revolution.work_list:
	# 	# 	self.game_options[self.games[0]]['work_list_listbox'].insert('end', each_work)
	# 	# 	self.configure.common_config[self.games[0]]['work_list'].append(each_work)









	# 	game_index = 0
	# 	lyb_l2r_tab = LYBTERA.LYBTeraTab(
	# 		self.tab_frame[game_index+1],
	# 		self.configure,
	# 		self.game_options[self.games[game_index]], 
	# 		self.game_frame[self.games[game_index]],
	# 		self.width,
	# 		self.height
	# 		)
















	# 	# game_index = 1
	# 	# lyb_l2r_tab = LYBLIN2REV.LYBLineage2RevolutionTab(
	# 	# 	self.tab_frame[game_index+1],
	# 	# 	self.configure,
	# 	# 	self.game_options[self.games[game_index]], 
	# 	# 	self.game_frame[self.games[game_index]],
	# 	# 	self.width,
	# 	# 	self.height
	# 	# 	)



	# 	# 클랜즈: 달의 그림자

	# 	# game_index += 1
	# 	# lyb_clans_tab = LYBCLANS.LYBClansTab(
	# 	# 	self.tab_frame[game_index+1],
	# 	# 	self.configure,
	# 	# 	self.game_options[self.games[game_index]], 
	# 	# 	self.game_frame[self.games[game_index]],
	# 	# 	self.width,
	# 	# 	self.height
	# 	# 	)






	# 	# 열혈강호M

	# 	# game_index += 1
	# 	# lyb_yeolhyul_tab = LYBYEOLHYUL.LYBYeolhyulTab(
	# 	# 	self.tab_frame[game_index+1],
	# 	# 	self.configure,
	# 	# 	self.game_options[self.games[game_index]], 
	# 	# 	self.game_frame[self.games[game_index]],
	# 	# 	self.width,
	# 	# 	self.height
	# 	# 	)

	# 	# self.game_options[self.games[0]]['window_list_option_menu'] = tkinter.OptionMenu(

	# 	# 	self.game_frame[self.games[0]]['window_list'],
	# 	# 	self.game_options[self.games[0]]['window_list_stringvar'],
	# 	# 	''

	# 	# 	)

	# 	# self.game_options[self.games[0]]['window_list_stringvar'].trace('w', lambda *args: self.select_window_list(args, game_name=self.games[0]))
	# 	# self.game_options[self.games[0]]['window_list_option_menu'].configure(width=18)
	# 	# self.game_options[self.games[0]]['window_list_option_menu'].pack(side=tkinter.TOP)

	# 	# self.game_frame[self.games[0]]['window_list'].place(	

	# 	# 	x  					= 6*lybconstant.LYB_PADDING + self.width*0.5,
	# 	# 	y 					= 2*lybconstant.LYB_PADDING,
	# 	# 	width 				= self.width*0.5 - 10*lybconstant.LYB_PADDING,
	# 	# 	height 				= lybconstant.LYB_BUTTON_HEIGHT

	# 	# 	)

	# 	# -- 워크 리스트 라벨
	# 	# w_name = 'work_list_label'
	# 	# self.game_frame[self.games[0]][w_name] = ttk.Frame(self.tab_frame[-1], relief=frame_relief)
	# 	# self.game_options[self.games[0]][w_name] = ttk.Label(

	# 	# 	master				= self.game_frame[self.games[0]][w_name] ,
	# 	# 	text 				= lybconstant.LYB_LABEL_AVAILABLE_WORK_LIST,
	# 	# 	relief				= 'flat',

	# 	# 	)

	# 	# self.game_options[self.games[0]][w_name].pack(side=tkinter.TOP)
	# 	# self.game_frame[self.games[0]][w_name].place(	

	# 	# 	x  					= 2*lybconstant.LYB_PADDING,
	# 	# 	y 					= lybconstant.LYB_BUTTON_HEIGHT + 4*lybconstant.LYB_PADDING,
	# 	# 	width 				= self.width*0.5 - 10*lybconstant.LYB_PADDING,
	# 	# 	height 				= lybconstant.LYB_BUTTON_HEIGHT

	# 	# 	)

	# 	# -- 스케쥴 리스트 라벨
	# 	# w_name = 'schedule_list_label'
	# 	# self.game_frame[self.games[0]][w_name] = ttk.Frame(self.tab_frame[-1], relief=frame_relief)
	# 	# self.game_options[self.games[0]][w_name] = ttk.Label(

	# 	# 	master				= self.game_frame[self.games[0]][w_name] ,
	# 	# 	text 				= lybconstant.LYB_LABEL_SCHEDULE_WORK_LIST,
	# 	# 	relief				= 'flat',

	# 	# 	)
	# 	# self.game_options[self.games[0]][w_name].pack(side=tkinter.TOP)
	# 	# self.game_frame[self.games[0]][w_name].place(	

	# 	# 	x  					= 6*lybconstant.LYB_PADDING + self.width*0.5,
	# 	# 	y 					= lybconstant.LYB_BUTTON_HEIGHT + 4*lybconstant.LYB_PADDING,
	# 	# 	width 				= self.width*0.5 - 10*lybconstant.LYB_PADDING,
	# 	# 	height 				= lybconstant.LYB_BUTTON_HEIGHT

	# 	# 	)

	# 	# -- 작업 목록
	# 	# self.game_frame[self.games[0]]['work_list'] = ttk.Frame(self.tab_frame[-1], relief=frame_relief)
	# 	# self.game_options[self.games[0]]['work_list_listbox'] = tkinter.Listbox(

	# 	# 	master				= self.game_frame[self.games[0]]['work_list'], 
	# 	# 	font				= ("돋움", 10),
	# 	# 	activestyle			= 'none'

	# 	# 	)
	# 	# self.game_options[self.games[0]]['work_list_listbox'].pack(side=tkinter.TOP)
	# 	# self.game_options[self.games[0]]['work_list_listbox'].bind(
	# 	# 	'<<ListboxSelect>>', 
	# 	# 	lambda event: self.select_work_list(event, game_name=self.games[0])
	# 	# 	)

	# 	# self.game_frame[self.games[0]]['work_list'].place(	

	# 	# 	x  					= 2 * lybconstant.LYB_PADDING,
	# 	# 	y 					= 2 * lybconstant.LYB_BUTTON_HEIGHT + 4 * lybconstant.LYB_PADDING,
	# 	# 	width 				= self.width*0.5 - 10*lybconstant.LYB_PADDING,
	# 	# 	height 				= self.height*0.4

	# 	# 	)


	# 	# -- 스케쥴 목록
	# 	# self.game_frame[self.games[0]]['schedule_list'] = ttk.Frame(self.tab_frame[-1], relief=frame_relief)
	# 	# self.game_options[self.games[0]]['schedule_list_listbox'] = tkinter.Listbox(

	# 	# 	master				= self.game_frame[self.games[0]]['schedule_list'],
	# 	# 	font				= ("돋움", 10),
	# 	# 	activestyle			= 'none'

	# 	# 	)
	# 	# self.game_options[self.games[0]]['schedule_list_listbox'].pack(side=tkinter.TOP)
	# 	# self.game_options[self.games[0]]['schedule_list_listbox'].bind(
	# 	# 	'<<ListboxSelect>>', 
	# 	# 	lambda event: self.select_schedule_list(event, game_name=self.games[0])
	# 	# 	)

	# 	# self.game_frame[self.games[0]]['schedule_list'].place(	
			
	# 	# 	x  					= 6 * lybconstant.LYB_PADDING + self.width * 0.5,
	# 	# 	y 					= 2 * lybconstant.LYB_BUTTON_HEIGHT + 4 * lybconstant.LYB_PADDING,
	# 	# 	width 				= self.width * 0.5 - 10*lybconstant.LYB_PADDING,
	# 	# 	height 				= self.height * 0.4

	# 	# 	)
	# 	# self.configure.common_config[self.games[0]]['schedule_list'] = copy.deepcopy(self.configure.common_config[self.games[0]]['work_list'])

	# 	# for each_work in self.configure.common_config[self.games[0]]['schedule_list']:
	# 	# 	self.game_options[self.games[0]]['schedule_list_listbox'].insert('end', each_work)


	# 	# self.game_frame[self.games[0]]['options'] = ttk.Frame(self.tab_frame[-1], relief=frame_relief)

	# 	# self.game_frame[self.games[0]]['left_option'] = ttk.Frame(
	# 	# 	master 				= self.game_frame[self.games[0]]['options'],
	# 	# 	relief 				= frame_relief
	# 	# 	)
	# 	# self.game_frame[self.games[0]]['left_option'].place(
	# 	# 	x 					= 2 * lybconstant.LYB_PADDING,
	# 	# 	y					= 2 * lybconstant.LYB_PADDING,
	# 	# 	width 				= self.width * 0.5 - 8 * lybconstant.LYB_PADDING,
	# 	# 	height 				= self.height * 0.4 - 4 * lybconstant.LYB_PADDING
	# 	# 	)
	# 	# self.game_frame[self.games[0]]['right_option'] = ttk.Frame(
	# 	# 	master 				= self.game_frame[self.games[0]]['options'],
	# 	# 	relief 				= frame_relief
	# 	# 	)
	# 	# self.game_frame[self.games[0]]['right_option'].place(
	# 	# 	x 					= self.width * 0.5,
	# 	# 	y					= 2 * lybconstant.LYB_PADDING,
	# 	# 	width 				= self.width * 0.5 - 8 * lybconstant.LYB_PADDING,
	# 	# 	height 				= self.height * 0.4 - 4 * lybconstant.LYB_PADDING
	# 	# 	)

	# 	# self.game_frame[self.games[0]]['options'].place(	
			
	# 	# 	x  					= 2 * lybconstant.LYB_PADDING,
	# 	# 	y 					= self.height * 0.5,
	# 	# 	width 				= self.width - 6 * lybconstant.LYB_PADDING,
	# 	# 	height 				= self.height * 0.4 + 10 * lybconstant.LYB_PADDING

	# 	# 	)

	# 	self.note.pack()

	# 	# self.master.bind('<Return>', lambda event, a=0:
	# 		 # self.searchWindow(a))

	# 	self.master.bind('<Return>', self.searchWindow)
	# 	self.master.bind('<F1>', self.startWorker)
	# 	self.master.bind('<F2>', self.pauseWorker)
	# 	self.master.bind('<F3>', self.terminateWorker)

	# 	# -----------------------------------------------------
	# 	# Thread variable
	# 	# -----------------------------------------------------

	# 	self.workers = []		
	# 	self.worker_dic = {}
	# 	self.recovery_count_dic = {}
	# 	self.hwnds = {}
	# 	self.side_hwnds = {}
	# 	self.parent_hwnds = {}
	# 	self.start_flag = 0x00
	# 	# self.ready_to_start = False

	# 	self.manage_workers()


	# def manage_workers(self):
	# 	# self.information.insert("end", time.ctime() + "\n")
	# 	# self.information.see("end")
	# 	# print('[DEBUG] REMOVE ME:', self.gui_config_dic[lybconstant.LYB_DO_STRING_INACTIVE_MODE_FLAG].get())
	# 	self.workers = [worker for worker in self.workers if worker.isAlive()]
	# 	# print('Thread count:', threading.activeCount())
	# 	for worker in self.workers:
	# 		while True:
	# 			try:
	# 				response_message = worker.response_queue.get_nowait()
	# 				# if worker.response_queue.qsize() < 1:
	# 				# 	print('task done')
	# 				worker.response_queue.task_done()
	# 				self.process_message(worker, response_message)
	# 			except queue.Empty:
	# 				break
	# 			except:
	# 				self.logging_message("FAIL", "Manager Exception: " +  str(sys.exc_info()[0]) + '(' +str(sys.exc_info()[1]) + ')')

	# 	self.update_monitor_master()

	# 	# if self.ready_to_start == True and self.search_flag == True:
	# 	# 	self.startWorker(None)

	# 	try:
	# 		period_update_ui = float(self.configure.common_config[lybconstant.LYB_DO_STRING_PERIOD_UPDATE_UI]) * 1000
	# 	except:
	# 		period_update_ui = 1000

	# 	# print(period_update_ui)
	# 	self.master.after(int(period_update_ui), self.manage_workers)

	# def process_message(self, worker, message):

	# 	if message.type == 'end_return':
	# 		self.logging_message('INFO', message.message + " 작업 종료")
	# 		if not message.message in self.worker_dic:
	# 			print('[DEBUG] MaxCountOfRecovery:', 
	# 				self.configure.common_config[lybconstant.LYB_DO_STRING_RECOVERY_COUNT])
	# 			max_recovery_count = self.configure.common_config[lybconstant.LYB_DO_STRING_RECOVERY_COUNT]
	# 			for window_name, worker_thread in self.worker_dic.items():
	# 				if worker == worker_thread:
	# 					if not window_name in self.recovery_count_dic:
	# 						self.recovery_count_dic[window_name] = 0

	# 					if self.recovery_count_dic[window_name] < max_recovery_count:
	# 						self.update_monitor_master()
	# 						self.logging_message('INFO', 
	# 							"[" + window_name+ "] 에서 에러 감지됨. 재실행 합니다." + 
	# 							str(self.recovery_count_dic[window_name] + 1) + '/' + str(max_recovery_count))
	# 						self.start_each_worker(window_name)
	# 						self.recovery_count_dic[window_name] += 1
	# 						break
	# 					else:
	# 						self.recovery_count_dic[window_name] = 0

	# 	elif message.type == 'search_hwnd_return':
	# 		self.hwnds = copy.deepcopy(message.message)
	# 	elif message.type == 'search_side_hwnd_return':
	# 		self.side_hwnds = copy.deepcopy(message.message)
	# 		print('DEBUGXXXXX:', self.side_hwnds)
	# 	elif message.type == 'search_parent_hwnd_return':
	# 		self.parent_hwnds = copy.deepcopy(message.message)
	# 		print('[parent handles]', self.parent_hwnds)
	# 	elif message.type == 'search_title_return':
	# 		self.search_window.delete(0, 'end')

	# 		if len(message.message) > 0:

	# 			for each_title in message.message:
	# 				self.logging_message("SUCCESS",str(each_title) + " 창 검색됨")
	# 				self.search_window.insert('end', each_title)
	# 				self.search_window.select_set('end')

	# 			self.selectedWindowList(None)
	# 			self.search_flag = True
	# 		else:
	# 			self.logging_message("FAIL", "[" + self.configure.keyword + "]" + " 단어가 포함된 창을 찾지 못했습니다")
	# 			self.logging_message("FAIL", "창 이름, 창 사이즈(640x360), 창이 최소화된 상태인지 확인해주세요")
	# 		self.refresh_window_game()
	# 	elif message.type == 'log':
	# 		self.logging_message(None, message.message)
	# 	elif message.type.upper() == 'GOOD':
	# 		self.logging_message("GOOD", message.message)
	# 	elif message.type.upper() == 'BAD':
	# 		self.logging_message("BAD", message.message)
	# 	elif message.type.upper() == 'NICE':
	# 		self.logging_message("NICE", message.message)
	# 	elif message.type.upper() == 'SUB':
	# 		self.logging_message("SUB", message.message)
	# 	elif message.type.upper() == 'INFO':
	# 		self.logging_message("INFO", message.message)
	# 	elif message.type == 'error':
	# 		self.logging_message("FAIL", message.message)
	# 	elif message.type == 'game_object':
	# 		game = message.message
	# 		self.game_object[game.window_title] = game
	# 		# self.terminateWorker(None)

	# def logging_message(self, tag, logging_message):

	# 	if len(self.configure.common_config[lybconstant.LYB_DO_STRING_LOG_FILTER]) > 0:
	# 		if not self.configure.common_config[lybconstant.LYB_DO_STRING_LOG_FILTER] in logging_message:
	# 			return

	# 	if int(self.information.index('end').split('.')[0]) > 1000000:
	# 	 	self.information.delete(1.0, tkinter.END)

	# 	self.information.insert("end", "[" + time.strftime("%H:%M:%S") + "] ", tag)
	# 	self.information.insert("end", logging_message + "\n", tag)
	# 	self.information.see("end")

	# def executeThread(self):

	# 	# if self.configure.common_config['security_code'] != lybconstant.LYB_SECURITY_CODE:
	# 	# 	self.logging_message('FAIL', '실행 인증 코드 [' + self.configure.common_config['security_code'] + '] 거부됨')
	# 	# 	self.security_authority = False
	# 	# 	return None
	# 	# else:
	# 	# 	if self.security_authority == False:
	# 	# 		self.logging_message('SUCCESS', '실행 인증 코드 [' + self.configure.common_config['security_code'] + '] 승인됨')
	# 	# 		self.security_authority = True


	# 	# license_limit = lybconstant.LYB_LICENSE_LIMIT - time.time()
	# 	try:
	# 		license_limit = likeyoubot_license.LYBLicense().read_license()
	# 		if license_limit > 0:
	# 			self.logging_message('SUCCESS', str(lybconstant.LYB_VERSION) + ' 라이센스가 ' + 
	# 				str(int(license_limit/(24*60*60))) + '일 ' + str(int((license_limit/(60*60))%24)) + '시간 ' + str(int((license_limit/60)%60)) + '분 후에 종료됩니다.')
	# 		else:
	# 			self.logging_message('FAIL', str(lybconstant.LYB_VERSION) + ' 라이센스가 종료 되었습니다. www.dogfooter.com 사이트에서 무료로 새버전을 다운로드 받으세요.')
	# 			return None
	# 	except:
	# 		self.logging_message('FAIL', str(lybconstant.LYB_VERSION) + ' 라이센스 정보를 찾을 수 없습니다. 라이센스 정보를 www.dogfooter.com 에서 확인하세요.')
	# 		return None
	
	# 	worker_thread = likeyoubot_worker.LYBWorker('Thread-'+str(self.start_flag), self.configure, queue.Queue(), queue.Queue())
	# 	worker_thread.daemon = True
	# 	worker_thread.start()
	# 	self.workers.append(worker_thread)

	# 	return worker_thread

	# def startWorker(self, e):

	# 	# if self.ready_to_start == False:			
	# 	# 	self.search_flag = False
	# 	# 	self.ready_to_start = True
	# 	# 	self.searchWindow(None)
	# 	# else:


	# 	for i in range(self.search_window.size()):
	# 		if not self.search_window.get(i) in self.configure.window_config:
	# 			self.configure.window_config[self.search_window.get(i)] = copy.deepcopy(self.configure.common_config)

	# 		for each_config, each_value in self.configure.common_config.items():
	# 			if not each_config in self.configure.window_config[self.search_window.get(i)]:
	# 				self.configure.window_config[self.search_window.get(i)][each_config] = self.configure.common_config[each_config]



	# 	items = map(int, self.search_window.curselection())
	# 	count = 0
	# 	for item in items:

	# 		#self.configure.common_config['threshold_entry'] = float(int(self.threshold_entry.get()) / 100)

	# 		# if float(self.pixel_tolerance_entry.get()) >= 50.0:
	# 		# 	self.pixel_tolerance_entry.delete(0, 'end')
	# 		# 	self.pixel_tolerance_entry.insert(0, '50.0')
	# 		# elif float(self.pixel_tolerance_entry.get()) <= 0.0:
	# 		# 	self.pixel_tolerance_entry.delete(0, 'end')
	# 		# 	self.pixel_tolerance_entry.insert(0, '0.0')

	# 		# self.configure.common_config['pixel_tolerance_entry'] = self.pixel_tolerance_entry.get()


	# 		# if int(self.wakeup_period_entry.get()) <= 0:
	# 		# 	self.wakeup_period_entry.set('1')

	# 		# self.configure.common_config['wakeup_period_entry'] = self.wakeup_period_entry.get()
			
	# 		# if float(self.wait_time_scene_change.get()) <= 0.0:
	# 		# 	self.wait_time_scene_change.delete(0, 'end')
	# 		# 	self.wait_time_scene_change.insert(0, '0')

	# 		# self.configure.common_config[lybconstant.LYB_DO_STRING_WAIT_TIME_SCENE_CHANGE] = self.wait_time_scene_change.get()

	# 		self.start_each_worker(self.search_window.get(item))
	# 		count += 1

	# 	if count == 0:
	# 		self.logging_message('FAIL', '작업을 수행할 창이 선택되지 않았습니다.' )
	# 		# self.ready_to_start = False
	# 		return


	# 	self.start_button.configure(stat='disabled')
	# 	# self.search_button.configure(stat='disabled')
	# 	self.keyword_entry.configure(stat='disabled')
	# 	# for i in range(len(self.tab_frame)):
	# 	# 		if i !=0:
	# 	# 			self.note.tab(i, stat='disabled')

	# 	# self.ready_to_start = False


	# 	try:
	# 		with open(self.configure.path, 'wb') as dat_file:
	# 			pickle.dump(self.configure, dat_file)
	# 	except:
	# 		self.logging_message("FAIL", 'toggleCheckBox Exception: ' + str(sys.exc_info()[0]) + '(' +str(sys.exc_info()[1]) + ')')
			
	# def start_each_worker(self, window_name):
	# 	if window_name in self.worker_dic:
	# 		print('DEBUG start: already started', window_name, self.worker_dic)
	# 		self.logging_message('INFO', window_name + ' 이미 실행 중입니다.' )
	# 		return

	# 	try:
	# 		each_hwnd = self.hwnds[window_name]
	# 	except:
	# 		self.logging_message('FAIL', '싱크 오류 발생!! 창을 검색한 후 다시 시작해주세요.' )
	# 		return

	# 	self.configure.common_config['threshold_entry'] = float(int(self.threshold_entry.get()) / 100)

	# 	started_window_name		= window_name
	# 	started_game_name		= self.configure.get_window_config(started_window_name, 'games')
	# 	if started_game_name in self.configure.window_config[started_window_name]:
	# 		started_option			= self.configure.get_window_config(started_window_name, started_game_name)
	# 	else:
	# 		started_option			= self.configure.common_config[started_game_name]

	# 	started_config 			= self.configure
	# 	started_window_config	= self.configure.window_config[started_window_name]

	# 	# if 'schedule_list' in self.configure.window_config[started_window_name]:
	# 	# 	started_option = self.configure.window_config[started_window_name]['schedule_list']

	# 	worker_thread = self.executeThread()
	# 	if worker_thread == None:
	# 		return

	# 	self.worker_dic[started_window_name] = worker_thread

	# 	side_window_handle = None
	# 	if each_hwnd in self.side_hwnds:
	# 		side_window_handle = self.side_hwnds[each_hwnd]

	# 	parent_window_handle = None
	# 	if each_hwnd in self.parent_hwnds:
	# 		parent_window_handle = self.parent_hwnds[each_hwnd]

	# 	worker_thread.command_queue.put_nowait(likeyoubot_message.LYBMessage('start', 
	# 			[
	# 				self.start_flag, 
	# 				each_hwnd, 
	# 				started_window_name,
	# 				started_game_name,
	# 				started_option,
	# 				started_config,
	# 				started_window_config,
	# 				side_window_handle,
	# 				parent_window_handle
	# 				]					
	# 			)
	# 		)
	# 	self.logging_message('INFO', window_name + ' 작업 시작' )

	# 	try:
	# 		with open(self.configure.path, 'wb') as dat_file:
	# 			pickle.dump(self.configure, dat_file)
	# 	except:
	# 		self.logging_message("FAIL", 'toggleCheckBox Exception: ' + str(sys.exc_info()[0]) + '(' +str(sys.exc_info()[1]) + ')')

	# def pause_each_worker(self, window_name):
	# 	if not window_name in self.worker_dic:
	# 		print('DEBUG pause: Not found worker', window_name, self.worker_dic)
	# 		return

	# 	worker = self.worker_dic[window_name]

	# 	if worker.isAlive():
	# 		worker.command_queue.put_nowait(likeyoubot_message.LYBMessage('pause', None))

	# def terminate_each_worker(self, window_name):
	# 	if not window_name in self.worker_dic:
	# 		print('DEBUG terminate: Not found worker', window_name, self.worker_dic)
	# 		return

	# 	worker = self.worker_dic[window_name]

	# 	if worker.isAlive():
	# 		worker.command_queue.put_nowait(likeyoubot_message.LYBMessage('end', None))

	# def pauseWorker(self, e):
	# 	if len(self.workers) < 1:
	# 		return
			
	# 	if self.pause_button['text'] == '일시정지\n[F2]':
	# 		self.pause_button.configure(text='다시시작\n[F2]')
	# 	else:
	# 		self.pause_button.configure(text='일시정지\n[F2]')

	# 	for worker in self.workers:
	# 		worker.command_queue.put_nowait(likeyoubot_message.LYBMessage('pause', None))

	# def terminateWorker(self, e):
	# 	for worker in self.workers:
	# 		worker.command_queue.put_nowait(likeyoubot_message.LYBMessage('end', None))
	# 	self.start_button.configure(stat='normal')
	# 	# self.search_button.configure(stat='normal')
	# 	self.keyword_entry.configure(stat='normal')

	# 	if self.pause_button['text'] != '일시정지\n[F2]':
	# 		self.pause_button.configure(text='일시정지\n[F2]')

	# 	# for i in range(len(self.tab_frame)):
	# 	# 	if i !=0:
	# 	# 		self.note.tab(i, stat='normal')

	# def searchWindow(self, e):
	# 	self.configure.keyword = self.keyword_entry.get()

	# 	try:
	# 		with open(self.configure.path, 'wb') as dat_file:
	# 			pickle.dump(self.configure, dat_file)
	# 	except:
	# 		self.logging_message("FAIL", 'searchWindow Exception: ' +  str(sys.exc_info()[0]) + '(' +str(sys.exc_info()[1]) + ')')

	# 	worker_thread = self.executeThread()
	# 	if worker_thread == None:
	# 		return

	# 	worker_thread.command_queue.put_nowait(likeyoubot_message.LYBMessage('search', self.configure.keyword))

	# def callback_inactive_mode_flag_stringvar(self, args):
	# 	self.set_config(lybconstant.LYB_DO_STRING_INACTIVE_MODE_FLAG)

	# def callback_use_inactive_mode_booleanvar(self):
	# 	self.set_config(lybconstant.LYB_DO_BOOLEAN_USE_INACTIVE_MODE)

	# def toggleCommonCheckBox(self, value):
	# 	self.set_config(value)

	# def toggle_debug_checkbox(self, value):
	# 	self.set_config(value)

	# def selectedWindowList(self, event):
	# 	# print(self.configure.window_config)
	# 	# print(self.configure.common_config)
	# 	# self.search_window.selection_clear(self.search_window.size() - 1)
	# 	# self.search_window.selection_clear( 0 )
	# 	print('[DEBUG] selectedWindowList 1')

	# 	if event != None:
	# 		if self.note.tk.call(self.note._w, "identify", "tab", event.x, event.y) != 0:
	# 			print('[DEBUG] selectedWindowList', self.note.tk.call(self.note._w, "identify", "tab", event.x, event.y))
	# 			return

	# 	if len(self.search_window.curselection()) == 0 and len(self.selected_window_list) > 0 and self.is_clicked_common_tab == True:
	# 		for each_window in self.selected_window_list:
	# 			for i in range(self.search_window.size()):
	# 				if each_window == self.search_window.get(i):
	# 					self.search_window.select_set(i)
	# 		self.is_clicked_common_tab == False
	# 		# 성능상 이슈로 return 추가함
	# 		return

	# 	items = map(int, self.search_window.curselection())
	# 	c_label = ''

	# 	count = 0
	# 	for item in items:

	# 		# if self.search_window.get(item) == '':
	# 		# 	continue
	# 		if not '...' in c_label:
	# 			if not c_label == '':
	# 				c_label += ', '
	# 			if len(c_label + self.search_window.get(item)) > 20:
	# 				c_label += '...'
	# 			else:
	# 				c_label += self.search_window.get(item)
	# 		count += 1


	# 	if c_label == '':
	# 		c_label = lybconstant.LYB_LABEL_SELECT_WINDOW_TEXT	
	# 	elif count > 1 and count == self.search_window.size():
	# 		c_label = lybconstant.LYB_LABEL_SELECTED_ALL

	# 	# print('count=', count, 'search_window=', self.search_window.size(), c_label)


	# 	self.configure_label.configure(
	# 		text 				= c_label
	# 		)

	# 	for each_config, each_value in self.gui_config_dic.items():
	# 		is_selected = False
	# 		items = map(int, self.search_window.curselection())

	# 		for item in items:
	# 			is_selected = True
	# 			# if self.search_window.get(item) == '':
	# 			# 	continue
	# 			window_name = self.search_window.get(item) 
	# 			if window_name in self.configure.window_config:
	# 				if self.configure.get_window_config(window_name, each_config) != self.gui_config_dic[each_config].get():
	# 					self.gui_config_dic[each_config].set(self.configure.get_window_config(window_name, each_config))

	# 		if is_selected == False:
	# 			if self.configure.common_config[each_config] != self.gui_config_dic[each_config].get():
	# 				self.gui_config_dic[each_config].set(self.configure.common_config[each_config])

	# def selected_game(self, args):

	# 	if len(self.search_window.curselection()) > 0:
	# 		self.selected_window_list = []
	# 		items = map(int, self.search_window.curselection())
	# 		for i in items:
	# 			self.selected_window_list.append(self.search_window.get(i))
	# 	# print('[DEBUG] 1:', self.selected_window_list)
	# 	self.set_config('games')
	# 	# print('[DEBUG] 2:', self.selected_window_list)		

	# 	self.is_clicked_common_tab = True
	# 	self.selectedWindowList(None)

	# def set_config(self, value):

	# 	# print('[COMMON CONFIG]', value, self.configure.common_config[value])

	# 	items = map(int, self.search_window.curselection())

	# 	is_selected = False
	# 	for item in items:
	# 		is_selected = True
	# 		self.configure.set_window_config(self.search_window.get(item), value, self.gui_config_dic[value].get())

	# 	# if is_selected == False:
	# 	# 	# 공통적용
	# 	# 	self.configure.common_config[value] = self.gui_config_dic[value].get()
		

	# 	# print(self.configure.window_config)
	# 	# print(self.configure.common_config)

	# 	self.refresh_window_game()

	# 	try:
	# 		with open(self.configure.path, 'wb') as dat_file:
	# 			pickle.dump(self.configure, dat_file)
	# 	except:
	# 		self.logging_message("FAIL", 'set_config Exception: ' + sys.exc_info()[0])

	# # def select_window_list(self, *args, game_name):
	# # 	# TODO: 같은 게임안에서 윈도우마다 다르게 설정가능
		
	# # 	selected_window_name = self.game_options[game_name]['window_list_stringvar'].get()
	# # 	if len(selected_window_name) > 0:
	# # 		if not game_name in self.configure.window_config[selected_window_name]:
	# # 			self.configure.window_config[selected_window_name][game_name] = copy.deepcopy(self.configure.common_config[game_name])
	# # 			print('DEBUGXX', selected_window_name, game_name, self.configure.window_config[selected_window_name][game_name])

	# # 		self.set_game_config(game_name)

	# def refresh_window_game(self):



	# 	if self.gui_config_dic[lybconstant.LYB_DO_BOOLEAN_USE_INACTIVE_MODE].get() == True:
	# 		self.inactive_flag_option_menu.configure(stat=tkinter.NORMAL)
	# 	else:
	# 		self.inactive_flag_option_menu.configure(stat=tkinter.DISABLED)
	# 	# 사용자가 윈도우를 검색하면 각 게임탭에서 어떤 윈도우에 어떤 게임을 실행할 지에 대한 정보를 채워준다.
	# 	# 게임 탭에서는 게임을 키로 윈도우 리스트를 붙여준다.
	# 	for each_game in self.games:
	# 		if not each_game in self.game_options:
	# 			continue

	# 		# print('DEBUGXX --- 1')
	# 		self.game_options[each_game]['window_list_stringvar'].set('')
	# 		# self.game_options[each_game]['window_list_option_menu']['menu'].delete(0, 'end')
	# 		# print('DEBUGXX --- 1')

	# 		new_window_list = []
	# 		for i in range(self.search_window.size()):
	# 			game_name = self.configure.get_window_config(self.search_window.get(i), 'games')
	# 			if each_game == game_name:
	# 				new_window_list.append(self.search_window.get(i))

	# 		self.game_options[each_game]['window_list_option_menu']['values'] = new_window_list
	# 		if len(new_window_list) > 0:
	# 			self.game_options[each_game]['window_list_stringvar'].set(new_window_list[0])
	# 		# if len(new_window_list) == 0:
	# 		# 	self.game_options[each_game]['window_list_option_menu']['menu'].add_command(
	# 		# 		label 				= '', 
	# 		# 		command 			= tkinter._setit(self.game_options[each_game]['window_list_stringvar'], '')
	# 		# 		)
	# 		# else:
	# 		# 	self.game_options[each_game]['window_list_stringvar'].set(new_window_list[0])
	# 		# 	for each_window in new_window_list:
	# 		# 		self.game_options[each_game]['window_list_option_menu']['menu'].add_command(
	# 		# 			label 				= each_window, 
	# 		# 			command 			= tkinter._setit(self.game_options[each_game]['window_list_stringvar'], each_window)
	# 		# 			)

	# def get_game_schedule_list(self, game_name):

	# 	window_name = self.game_options[game_name]['window_list_stringvar'].get()

	# 	if len(window_name) > 0:
	# 		if not game_name in self.configure.window_config[window_name]:
	# 			self.configure.window_config[window_name][game_name] = copy.deepcopy(self.configure.common_config[game_name])

	# 		schedule_list = self.configure.window_config[window_name][game_name]['schedule_list']
	# 	else:
	# 		schedule_list = self.configure.common_config[game_name]['schedule_list']

	# 	return schedule_list

	# # 리스트박스를 클릭하면 선택되는 거 같다. 그래서 마지막에 공백을 넣었다.
	# # def select_work_list(self, event, game_name):
	# # 	last_index = self.game_options[game_name]['work_list_listbox'].size() - 1
	# # 	self.game_options[game_name]['work_list_listbox'].selection_clear(last_index)

	# # 	schedule_list = self.get_game_schedule_list(game_name)

	# # 	if len(self.game_options[game_name]['work_list_listbox'].curselection()) > 0:
	# # 		item_index = self.game_options[game_name]['work_list_listbox'].curselection()[0]
	# # 		# 공백이면 리턴
	# # 		if item_index == last_index:
	# # 			return

	# # 		selected_work_name = self.game_options[game_name]['work_list_listbox'].get(item_index)
		
	# # 		print('DEBUG88:', self.game_options[game_name]['schedule_list_listbox'].size())

	# # 		if not selected_work_name in schedule_list:
	# # 			#schedule_list.append(selected_work_name)
	# # 			schedule_list.insert(len(schedule_list) - 1, selected_work_name)
	# # 			self.game_options[game_name]['schedule_list_listbox'].insert(self.game_options[game_name]['schedule_list_listbox'].size() - 1, selected_work_name)
					

	# # def select_schedule_list(self, event, game_name):

	# # 	last_index = self.game_options[game_name]['schedule_list_listbox'].size() - 1
	# # 	self.game_options[game_name]['schedule_list_listbox'].selection_clear(last_index)

	# # 	schedule_list = self.get_game_schedule_list(game_name)

	# # 	if len(self.game_options[game_name]['schedule_list_listbox'].curselection()) > 0:
	# # 		item_index = self.game_options[game_name]['schedule_list_listbox'].curselection()[0]
	# # 		if item_index == last_index:
	# # 			return
	# # 		selected_schedule_work_name = self.game_options[game_name]['schedule_list_listbox'].get(item_index)
	# # 		schedule_list.remove(selected_schedule_work_name)
	# # 		self.game_options[game_name]['schedule_list_listbox'].delete(item_index)
	# # 		print('DEBUG77:', self.game_options[game_name]['schedule_list_listbox'].size())

	# # 	window_name = self.game_options[game_name]['window_list_stringvar'].get()

	# # TODO: Game 설정 갱신 함수
	# def set_game_config(self, game_name):
	# 	size = self.game_options[game_name]['schedule_list_listbox'].size()
	# 	self.game_options[game_name]['schedule_list_listbox'].delete(0, size - 1)
		
	# 	schedule_work_list = self.get_game_schedule_list(game_name)

	# 	window_name = self.game_options[game_name]['window_list_stringvar'].get()

	# 	for each_work in schedule_work_list:
	# 		self.game_options[game_name]['schedule_list_listbox'].insert('end', each_work)

	# def clicked_tab(self, event):
	# 	print('[DEBUG] clicked_tab')
	# 	s = time.time()
	# 	tab_index = self.note.tk.call(self.note._w, "identify", "tab", event.x, event.y)

	# 	self.is_clicked_common_tab = False
	# 	if tab_index != 0:
	# 		if len(self.search_window.curselection()) > 0:
	# 			self.selected_window_list = []
	# 			items = map(int, self.search_window.curselection())
	# 			for i in items:
	# 				self.selected_window_list.append(self.search_window.get(i))
	# 	else:
	# 		if len(self.selected_window_list) > 0:
	# 			self.is_clicked_common_tab = True
	# 			# 이 부분을 주석 처리하면 선택된 윈도우들이 사라진다. 일단 주석 처리해보자.
	# 			self.selectedWindowList(None)
	# 	e = time.time()
	# 	print('[DEBUG] clicked_tab', round(e-s,2))

	# def callback_security_code_stringvar(self, args):
	# 	print(self.security_code.get())
	# 	self.configure.common_config['security_code'] = self.security_code.get()

	# def callback_hompage(self, event):
	# 	webbrowser.open_new(r"http://www.dogfooter.com")

	# def callbac_bitbucket(self, event):
	# 	webbrowser.open_new(r"https://bitbucket.org/dogfooter/dogfooter/src")

	# def callback_wakeup_period_entry(self, args):

	# 	try:
	# 		if len(self.wakeup_period_entry.get()) < 0:
	# 			wakeup_period_entry = 0
	# 		else:
	# 			wakeup_period_entry = float(self.wakeup_period_entry.get())

	# 		# if wakeup_period_entry < 0:
	# 		# 	self.wakeup_period_entry.set('0')

	# 		self.configure.common_config['wakeup_period_entry'] = wakeup_period_entry
	# 	except:
	# 		self.configure.common_config['wakeup_period_entry'] = 1.0

	# 	# print(self.configure.common_config['wakeup_period_entry'])

	# def callback_use_monitoring_booleanvar(self, args):
	# 	use_monitoring_flag = self.use_monitoring_flag.get()

	# 	self.configure.common_config[lybconstant.LYB_DO_BOOLEAN_USE_MONITORING] = use_monitoring_flag

	# 	# print(self.configure.common_config[lybconstant.LYB_DO_BOOLEAN_USE_MONITORING])

	# def callback_update_period_ui_entry(self, args):

	# 	try:
	# 		if len(self.update_period_ui_entry.get()) < 0:
	# 			update_period_ui = 0
	# 		else:
	# 			update_period_ui = float(self.update_period_ui_entry.get())

	# 		# if update_period_ui < 0:
	# 		# 	self.update_period_ui_entry.set('0')

	# 		self.configure.common_config[lybconstant.LYB_DO_STRING_PERIOD_UPDATE_UI] = update_period_ui
	# 	except:
	# 		self.configure.common_config[lybconstant.LYB_DO_STRING_PERIOD_UPDATE_UI] = float(1.0)

	# 	# print(self.configure.common_config[lybconstant.LYB_DO_STRING_PERIOD_UPDATE_UI])


	# def callback_close_app_stringvar(self, args):

	# 	if len(self.close_app_stringvar.get()) < 1:
	# 		config_value = 0
	# 	else:
	# 		config_value = int(self.close_app_stringvar.get())

	# 	# if config_value <= 0:
	# 	# 	self.close_app_stringvar.set('0')

	# 	self.configure.common_config[lybconstant.LYB_DO_STRING_CLOSE_APP_COUNT] = config_value

	# 	# print(self.configure.common_config[lybconstant.LYB_DO_STRING_CLOSE_APP_COUNT])

	# def callback_recovery_count_stringvar(self, args):

	# 	if len(self.recovery_count_stringvar.get()) < 1:
	# 		config_value = 0
	# 	else:
	# 		config_value = int(self.recovery_count_stringvar.get())

	# 	# if config_value <= 0:
	# 	# 	self.recovery_count_stringvar.set('0')

	# 	self.configure.common_config[lybconstant.LYB_DO_STRING_RECOVERY_COUNT] = config_value

	# 	# print(self.configure.common_config[lybconstant.LYB_DO_STRING_RECOVERY_COUNT])

	# def callback_wait_time_scene_change(self, args):

	# 	if len(self.wait_time_scene_change.get()) < 1:
	# 		wait_time_scene_change = 0
	# 	else:
	# 		wait_time_scene_change = int(self.wait_time_scene_change.get())

	# 	# if wait_time_scene_change <= 0:
	# 	# 	self.wait_time_scene_change.set('0')

	# 	self.configure.common_config[lybconstant.LYB_DO_STRING_WAIT_TIME_SCENE_CHANGE] = wait_time_scene_change

	# 	# print(self.configure.common_config[lybconstant.LYB_DO_STRING_WAIT_TIME_SCENE_CHANGE])

	# def callback_threshold_entry(self, args):

	# 	if len(self.threshold_entry.get()) < 1:
	# 		threshold_entry = 0.7
	# 	else:
	# 		threshold_entry = float(self.threshold_entry.get()) / 100.0

	# 	# if threshold_entry <= 0:
	# 	# 	self.threshold_entry.set('0')
	# 	# elif threshold_entry > 100:
	# 	# 	self.threshold_entry.set('100')

	# 	self.configure.common_config['threshold_entry'] = threshold_entry

	# 	# print(self.configure.common_config['threshold_entry'])

	# def callback_pixel_tolerance_entry(self, args):

	# 	if len(self.pixel_tolerance_entry.get()) < 1:
	# 		pixel_tolerance_entry = 30
	# 	else:
	# 		pixel_tolerance_entry = int(self.pixel_tolerance_entry.get())

	# 	if pixel_tolerance_entry > 255:
	# 		pixel_tolerance_entry = 255

	# 	# if pixel_tolerance_entry <= 0:
	# 	# 	self.pixel_tolerance_entry.set('0')
	# 	# elif pixel_tolerance_entry > 255:
	# 	# 	self.pixel_tolerance_entry.set('255')

	# 	self.configure.common_config['pixel_tolerance_entry'] = pixel_tolerance_entry

	# 	# print(self.configure.common_config['pixel_tolerance_entry'])

	# def callback_adjust_entry(self, args):

	# 	if len(self.adjust_entry.get()) < 1:
	# 		adjust_entry = 10
	# 	else:
	# 		adjust_entry = int(self.adjust_entry.get())

	# 	# if adjust_entry <= 0:
	# 	# 	self.adjust_entry.set('0')
	# 	# elif adjust_entry > 100:
	# 	# 	self.adjust_entry.set('100')

	# 	if adjust_entry > 100:
	# 		adjust_entry = 100

	# 	self.configure.common_config['adjust_entry'] = adjust_entry

	# 	# print(self.configure.common_config['adjust_entry'])

	# def callback_log_filter_entry_stringvar(self, args):
	# 	self.configure.common_config[lybconstant.LYB_DO_STRING_LOG_FILTER] = self.log_filter_entry.get()

	# def update_monitor_master(self):
	# 	# print('DEBUG:', self.worker_dic)

	# 	remove_list = []
	# 	for key, value in self.worker_dic.items():
	# 		if self.worker_dic[key].isAlive() == False:
	# 			remove_list.append(key)
	# 			if key in self.game_object:
	# 				self.game_object.pop(key)

	# 	for each_remove in remove_list:
	# 		self.worker_dic.pop(each_remove)

	# 	remove_list = []
	# 	if self.configure.common_config[lybconstant.LYB_DO_BOOLEAN_USE_MONITORING] == False:
	# 		for key, value in self.option_dic.items():
	# 			if '_monitor' in key:
	# 				self.option_dic[key].pack_forget()
	# 				window_name = key.split('_')[0]
	# 				remove_list.append(window_name + '_monitor')

	# 		for each_remove in remove_list:
	# 			self.option_dic.pop(each_remove)

	# 		return
					
	# 	remove_list = []
	# 	for key, value in self.option_dic.items():
	# 		if '_monitor' in key:
	# 			window_name = key.split('_')[0]

	# 			is_there = False
	# 			for i in range(self.search_window.size()):
	# 				if window_name == self.search_window.get(i):
	# 					is_there = True

	# 			if is_there == False:
	# 				self.option_dic[window_name + '_monitor'].pack_forget()
	# 				remove_list.append(window_name + '_monitor')


	# 	for each_remove in remove_list:
	# 		self.option_dic.pop(each_remove)

	# 	for i in range(self.search_window.size()):
	# 		window_name = self.search_window.get(i)

	# 		if not window_name + '_monitor' in self.option_dic:
	# 			self.option_dic[window_name + '_monitor'] = self.add_monitor_master_frame(
	# 															arg1=window_name, 
	# 															arg2='',
	# 															arg3='',
	# 															arg4='',
	# 															arg5='',
	# 															arg6='',
	# 															arg7='',
	# 															arg8='',
	# 															arg9='',
	# 															arg10='',
	# 															arg109='',
	# 															arg110='',
	# 															arg111='',
	# 															arg112='',
	# 															arg113=''
	# 															)

	# 		try:
	# 			if window_name in self.game_object:
	# 				game_object = self.game_object[window_name]

	# 				total_elapsed_time = time.time() - game_object.start_time
	# 				restart_period = int(game_object.get_game_config(lybconstant.LYB_GAME_TERA, lybconstant.LYB_DO_STRING_PERIOD_RESTART_GAME)) * 60

	# 				if 'loc' in game_object.last_click:
	# 					click_loc = self.adjust_monitor_name(game_object.get_adjusted_name(str(game_object.last_click['loc'])))
	# 				else:
	# 					click_loc = ''

	# 				scene_name = self.adjust_monitor_name(game_object.get_adjusted_name(game_object.current_matched_scene['name']), adj_length=10)
	# 				if len(scene_name) > 0:
	# 					scene_rate = '(' + str(game_object.current_matched_scene['rate']) + '%)'
	# 					scene_status = game_object.get_scene(game_object.current_matched_scene['name']).status
	# 					scene_name += scene_rate
	# 				else:
	# 					scene_rate = ''
	# 					scene_status = ''

	# 				if game_object.main_scene and game_object.current_schedule_work:
						
	# 					wlist = game_object.get_game_config(game_object.game_name, 'schedule_list')
	# 					work_name = game_object.current_schedule_work

	# 					# if work_name in game_object.main_scene.move_status:
	# 					# 	work_index = game_object.main_scene.move_status[work_name]
	# 					# 	try:
	# 					# 		work_name = game_object.main_scene.get_game_config('schedule_list')[work_index - 1]
	# 					# 	except:
	# 					# 		work_name = ''
	# 					# else:
	# 					work_index = game_object.main_scene.last_status[work_name]

	# 					new_work_name = str(work_index) + '. '  + game_object.current_schedule_work

	# 					if game_object.main_scene.get_option('hero_current_hp') == None:
	# 						hero_current_hp = ''
	# 					else:
	# 						hero_current_hp = str(game_object.main_scene.get_option('hero_current_hp'))

	# 					if game_object.main_scene.get_option('target_current_hp') == None:
	# 						target_current_hp = ''
	# 					else:
	# 						target_current_hp = str(game_object.main_scene.get_option('target_current_hp'))
						
	# 				else:
	# 					wlist = []
	# 					work_index = ''
	# 					new_work_name = ''

	# 					hero_current_hp = ''
	# 					target_current_hp = ''


	# 				self.update_monitor_master_frame(
	# 					self.option_dic[window_name + '_monitor'],
	# 					arg_list	= [
	# 									window_name, 
	# 									scene_name,
	# 									scene_status,
	# 									hero_current_hp,
	# 									target_current_hp,
	# 									# self.adjust_monitor_name(game_object.get_adjusted_name(game_object.current_matched_event['name'])),
	# 									# work_index,
	# 									'',
	# 									new_work_name,
	# 									'start',
	# 									click_loc,
	# 									str(time.strftime('%H:%M:%S', time.gmtime(int(restart_period - total_elapsed_time))))

	# 									],
	# 					wlist = wlist
	# 					)
	# 			else:				
	# 				self.update_monitor_master_frame(
	# 					self.option_dic[window_name + '_monitor'],
	# 					arg_list	= [
	# 									window_name, 	# 1
	# 									'',		# 2
	# 									'',		# 3
	# 									'',		# 4
	# 									'',		# 5
	# 									'',		# 6
	# 									'',		# 7
	# 									'stop',		# 8
	# 									'',		# 9
	# 									'',		# 10
	# 									]
	# 					)
	# 		except KeyError:
	# 			print("Exception1: " +  str(sys.exc_info()[0]) + '(' +str(sys.exc_info()[1]) + ')')
	# 			pass
	# 			print("Exception2: " +  str(sys.exc_info()[0]) + '(' +str(sys.exc_info()[1]) + ')')
	# 			pass
	# 		except:
	# 			print("Exception3: " +  str(sys.exc_info()[0]) + '(' +str(sys.exc_info()[1]) + ')')
	# 			return
	# 		# else:
	# 		# 	self.update_monitor_master_frame(
	# 		# 		self.option_dic[window_name + '_monitor']
	# 		# 		)



	# 	# for window_name, thread in self.worker_dic.items():
	# 	# 	if not window_name + '_monitor' in self.option_dic:
	# 	# 		self.option_dic[window_name + '_monitor'] = self.add_monitor_master(window_name)


	# def update_monitor_master_frame(self,
	# 	frame,
	# 	arg_list = [],
	# 	wlist =[]
	# 	):

	# 	label_list = frame.winfo_children()

	# 	i = 0
	# 	elapsed_time = 0
	# 	game_object = None
	# 	window_name = None
	# 	for each_arg in arg_list:
	# 		if i == 0:
	# 			text_arg = self.adjust_monitor_name(each_arg, adj_length=5)

	# 			window_name = each_arg
	# 		elif i == 3 or i == 4:
	# 			text_arg = each_arg
	# 			if len(text_arg) > 0:
	# 				s = ttk.Style()
	# 				s.configure('stable.TLabel', foreground='green')
	# 				s.configure('danger.TLabel', foreground='red')
	# 				s.configure('warning.TLabel', foreground='#bc750a')
	# 				hp = int(text_arg)
	# 				if hp > 75:
	# 					label_list[i].configure(style='stable.TLabel')
	# 				elif hp > 30 and hp <= 75:
	# 					label_list[i].configure(style='warning.TLabel')
	# 				else:
	# 					label_list[i].configure(style='danger.TLabel')

	# 				if hp == 0:
	# 					text_arg = ''
	# 		elif i == 6:	
	# 			if ( 	not window_name in self.current_work_dic or
	# 					self.current_work_dic[window_name] != each_arg
	# 					):
	# 				print('[DEBUG ----- ] wlist work:', self.wlist_stringvar_dic[window_name].get(),
	# 					 'game work:', each_arg)	
	# 				if window_name in self.current_work_dic:
	# 					print('[DEBUG +++++ ] current_work', self.current_work_dic[window_name])
	# 				self.current_work_dic[window_name] = each_arg
	# 				new_wlist = []
	# 				windex = 1
	# 				for each_w in wlist:
	# 					if len(each_w) < 1:
	# 						continue
	# 					new_wlist.append(str(windex) + '. ' + each_w)
	# 					windex += 1
	# 				self.wlist_combobox_dic[window_name]['values'] = new_wlist
	# 				self.wlist_stringvar_skip_dic[window_name] = True
	# 				self.wlist_stringvar_dic[window_name].set(each_arg)
	# 			# print('[DEBUG ----- ]', self.current_work_dic[window_name], 
	# 			# 	self.wlist_stringvar_dic[window_name].get(),
	# 			# 	 each_arg)

	# 			i += 1
	# 			continue
	# 		elif i == 7:

	# 			s = ttk.Style()
	# 			s.configure('stop.TLabel', foreground='red')
	# 			s.configure('start.TLabel', foreground='#10890c')

	# 			if each_arg == 'stop':
	# 				text_arg = '●'
	# 				label_list[i].configure(style='stop.TLabel')
	# 			elif each_arg == 'start':
	# 				if window_name != None:
	# 					if window_name in self.game_object:
	# 						game_object = self.game_object[window_name]

	# 					if game_object != None and game_object.interval != None:
	# 						bot_period = game_object.interval
	# 					else:
	# 						bot_period = float(self.configure.common_config['wakeup_period_entry'])

	# 					if not window_name in self.monitor_check_point:
	# 						self.monitor_check_point[window_name] = 0

	# 					elapsed_time = time.time() - self.monitor_check_point[window_name]
	# 					if elapsed_time > bot_period:
	# 						self.monitor_check_point[window_name] = time.time()
	# 				else:
	# 					bot_period = 1

	# 				if bot_period < 0.11:
	# 					bot_period = 0.15

	# 				update_ui_period = float(self.configure.common_config[lybconstant.LYB_DO_STRING_PERIOD_UPDATE_UI])
	# 				if bot_period < update_ui_period:
	# 					if elapsed_time > update_ui_period:
	# 						text_arg = '●'
	# 					else:
	# 						text_arg = ''
	# 				else:
	# 					if elapsed_time > bot_period:
	# 						text_arg = '●'
	# 					else:
	# 						text_arg = ''

	# 				label_list[i].configure(style='start.TLabel')
	# 			else:
	# 				text_arg = each_arg
	# 		elif i > 9:
	# 			continue
	# 		else:
	# 			text_arg = each_arg

	# 		label_list[i].config(text=text_arg)
	# 		i += 1

	# def add_monitor_master_frame(self,

	# 	arg1 		= 'TITLE',
	# 	arg2 		= 'SCENE',
	# 	arg3 		= 'STATUS',
	# 	arg4 		= 'HERO',
	# 	arg5 		= 'TARGET',
	# 	arg6		= '',
	# 	arg7 		= 'WORK',
	# 	arg8 		= '',
	# 	arg9		= 'CLICKED',
	# 	arg10 		= 'CLOSE TIME',
	# 	arg109	 	= None,
	# 	arg110		= None,
	# 	arg111	 	= None,
	# 	arg112		= None,
	# 	arg113		= None
	# 	):


	# 	s = ttk.Style()
	# 	s.configure('LYB.TFrame', background='#3f74c6')

	# 	frame_label = ttk.Frame(
	# 		master 				= self.option_dic['monitor_master'],
	# 		style 				= 'LYB.TFrame'
	# 		)

	# 	column_count = 0

	# 	label = ttk.Label(
	# 		master 				= frame_label,
	# 		text 				= self.adjust_monitor_name(arg1, adj_length=7),
	# 		anchor 				= tkinter.N,
	# 		justify 			= tkinter.CENTER,
	# 		width 				= 10
	# 		)
	# 	label.pack(side=tkinter.LEFT, fill=tkinter.Y)
	# 	column_count += 1

	# 	label = ttk.Label(
	# 		master 				= frame_label,
	# 		text 				= arg2, 
	# 		anchor 				= tkinter.N,
	# 		justify 			= tkinter.CENTER,
	# 		width 				= 17
	# 		)
	# 	label.pack(side=tkinter.LEFT, fill=tkinter.Y)
	# 	column_count += 1

	# 	label = ttk.Label(
	# 		master 				= frame_label,
	# 		text 				= arg3, 
	# 		anchor 				= tkinter.N,
	# 		justify 			= tkinter.CENTER,
	# 		width 				= 8
	# 		)
	# 	label.pack(side=tkinter.LEFT, fill=tkinter.Y)
	# 	column_count += 1

	# 	label = ttk.Label(
	# 		master 				= frame_label,
	# 		text 				= arg4, 
	# 		anchor 				= tkinter.N,
	# 		justify 			= tkinter.CENTER,
	# 		width 				= 6
	# 		)
	# 	label.pack(side=tkinter.LEFT, fill=tkinter.Y)
	# 	column_count += 1

	# 	label = ttk.Label(
	# 		master 				= frame_label,
	# 		text 				= arg5, 
	# 		anchor 				= tkinter.N,
	# 		justify 			= tkinter.CENTER,
	# 		width 				= 6
	# 		)
	# 	label.pack(side=tkinter.LEFT, fill=tkinter.Y)
	# 	column_count += 1

	# 	label = ttk.Label(
	# 		master 				= frame_label,
	# 		text 				= arg6,
	# 		anchor 				= tkinter.N,
	# 		justify 			= tkinter.CENTER,
	# 		width 				= 3
	# 		)
	# 	label.pack(side=tkinter.LEFT, fill=tkinter.Y)
	# 	column_count += 1

	# 	if arg7 == 'WORK':

	# 		label = ttk.Label(
	# 			master 				= frame_label,
	# 			text 				= arg7,
	# 			anchor 				= tkinter.N,
	# 			justify 			= tkinter.CENTER,
	# 			width 				= 20
	# 			)
	# 		label.pack(side=tkinter.LEFT, fill=tkinter.Y)

	# 	else:
	# 		combo_list = [
	# 			'없음'
	# 		]

	# 		self.wlist_stringvar_dic[arg1] = tkinter.StringVar(frame_label)
	# 		self.wlist_stringvar_dic[arg1].trace('w', 
	# 			lambda *args: self.callback_select_wlist_stringvar(args, option_name=arg1))

	# 		self.wlist_stringvar_dic[arg1].set(combo_list[0])
	# 		self.wlist_combobox_dic[arg1] = ttk.Combobox(
	# 			master 				= frame_label,
	# 			values				= combo_list, 
	# 			textvariable		= self.wlist_stringvar_dic[arg1], 
	# 			state 				= "readonly",
	# 			height				= 10,
	# 			width 				= 18,
	# 			font 				= lybconstant.LYB_FONT,
	# 			justify 			= tkinter.LEFT
	# 			)
	# 		self.wlist_combobox_dic[arg1].set(combo_list[0])
	# 		self.wlist_combobox_dic[arg1].pack(anchor=tkinter.W, side=tkinter.LEFT)
	# 	column_count += 1


	# 	label = ttk.Label(
	# 		master 				= frame_label,
	# 		text 				= arg8,
	# 		anchor 				= tkinter.N,
	# 		justify 			= tkinter.CENTER,
	# 		width 				= 4
	# 		)

	# 	label.pack(side=tkinter.LEFT, fill=tkinter.Y)
	# 	column_count += 1

	# 	label = ttk.Label(
	# 		master 				= frame_label,
	# 		text 				= arg9,
	# 		anchor 				= tkinter.N,
	# 		justify 			= tkinter.CENTER,
	# 		width 				= 10
	# 		)
	# 	label.pack(side=tkinter.LEFT, fill=tkinter.Y)
	# 	column_count += 1

	# 	label = ttk.Label(
	# 		master 				= frame_label,
	# 		text 				= arg10,
	# 		anchor 				= tkinter.N,
	# 		justify 			= tkinter.CENTER,
	# 		width 				= 11
	# 		)
	# 	label.pack(side=tkinter.LEFT, fill=tkinter.Y)
	# 	column_count += 1

	# 	s = ttk.Style()
	# 	s.configure('mouse_up.TLabel', foreground='black', background='#f7f796', relief='groove')
	# 	s.configure('mouse_down.TLabel', foreground='#f7f796', background='black', relief='groove')

	# 	if arg109 != None:
	# 		button = ttk.Label(
	# 			master 				= frame_label, 
	# 			text 				= '■', 
	# 			anchor 				= tkinter.N,
	# 			justify 			= tkinter.CENTER,
	# 			style 				= 'mouse_up.TLabel',
	# 			cursor 				= 'hand2',
	# 			width 				= 5
	# 			)
	# 		button.pack(side=tkinter.LEFT)
	# 		self.monitor_button_index[0] = column_count
	# 		button.bind('<Button-1>', lambda event: self.callback_monitoring_execute_worker(event, window_name=arg1, index=self.monitor_button_index[0]))

	# 	column_count += 1

	# 	if arg110 != None:
	# 		button = ttk.Label(
	# 			master 				= frame_label, 
	# 			text 				= '▶', 
	# 			anchor 				= tkinter.N,
	# 			justify 			= tkinter.CENTER,
	# 			style 				= 'mouse_up.TLabel',
	# 			cursor 				= 'hand2',
	# 			width 				= 5
	# 			)
	# 		button.pack(side=tkinter.LEFT)
	# 		self.monitor_button_index[1] = column_count
	# 		button.bind('<Button-1>', lambda event: self.callback_monitoring_execute_worker(event, window_name=arg1, index=self.monitor_button_index[1]))
	# 	column_count += 1

	# 	if arg111 != None:
	# 		button = ttk.Label(
	# 			master 				= frame_label, 
	# 			text 				= '∥', 
	# 			anchor 				= tkinter.N,
	# 			justify 			= tkinter.CENTER,
	# 			style 				= 'mouse_up.TLabel',
	# 			cursor 				= 'hand2',
	# 			width 				= 5
	# 			)
	# 		button.pack(side=tkinter.LEFT)
	# 		self.monitor_button_index[2] = column_count
	# 		button.bind('<Button-1>', lambda event: self.callback_monitoring_execute_worker(event, window_name=arg1, index=self.monitor_button_index[2]))
	# 	column_count += 1

	# 	if arg112 != None:
	# 		button = ttk.Label(
	# 			master 				= frame_label, 
	# 			text 				= '《', 
	# 			anchor 				= tkinter.N,
	# 			justify 			= tkinter.CENTER,
	# 			style 				= 'mouse_up.TLabel',
	# 			cursor 				= 'hand2',
	# 			width 				= 5
	# 			)
	# 		button.pack(side=tkinter.LEFT)
	# 		self.monitor_button_index[3] = column_count
	# 		button.bind('<Button-1>', lambda event: self.callback_monitoring_execute_worker(event, window_name=arg1, index=self.monitor_button_index[3]))
	# 	column_count += 1

	# 	if arg113 != None:
	# 		button = ttk.Label(
	# 			master 				= frame_label, 
	# 			text 				= '》', 
	# 			anchor 				= tkinter.N,
	# 			justify 			= tkinter.CENTER,
	# 			style 				= 'mouse_up.TLabel',
	# 			cursor 				= 'hand2',
	# 			width 				= 5
	# 			)
	# 		button.pack(side=tkinter.LEFT)
	# 		self.monitor_button_index[4] = column_count
	# 		button.bind('<Button-1>', lambda event: self.callback_monitoring_execute_worker(event, window_name=arg1, index=self.monitor_button_index[4]))
	# 	column_count += 1

	# 	frame_label.pack(anchor=tkinter.W, fill=tkinter.Y)

	# 	return frame_label

	# def adjust_monitor_name(self, name, adj_length=10):

	# 	if name == None:
	# 		return ''

	# 	if len(name) > adj_length:
	# 		return name[0:adj_length-3] + '...' + name[-1]

	# 	return name

	# def callback_monitoring_execute_worker(self, e, window_name, index):
	# 	stop_button_label = self.option_dic[window_name + '_monitor'].winfo_children()[index]
	# 	stop_button_label.configure(style='mouse_down.TLabel')
	# 	self.master.after(100, lambda :self.callback_monitoring_execute_worker_back(window_name=window_name, index=index))

	# def callback_monitoring_execute_worker_back(self, window_name, index):
	# 	stop_button_label = self.option_dic[window_name + '_monitor'].winfo_children()[index]
	# 	stop_button_label.configure(style='mouse_up.TLabel')
	# 	print('DEBUG: window_name=['+window_name+']['+str(index)+']')

	# 	if index == self.monitor_button_index[0]:
	# 		self.terminate_each_worker(window_name)
	# 	elif index == self.monitor_button_index[1]:
	# 		self.start_each_worker(window_name)
	# 	elif index == self.monitor_button_index[2]:
	# 		self.pause_each_worker(window_name)
	# 	elif index == self.monitor_button_index[3]:
	# 		self.backward_work_each_worker(window_name)
	# 	elif index == self.monitor_button_index[4]:
	# 		self.forward_work_each_worker(window_name)

	# def backward_work_each_worker(self, window_name):

	# 	game_object = self.game_object[window_name]
	# 	if game_object == None or game_object.main_scene == None:
	# 		return

	# 	work_name = game_object.current_schedule_work
	# 	if work_name != None:
	# 		try:
	# 			if not work_name in game_object.main_scene.move_status:
	# 				work_index = game_object.main_scene.last_status[work_name]
	# 			else:
	# 				work_index = game_object.main_scene.move_status[work_name]
	# 		except:
	# 			work_index = game_object.main_scene.last_status[work_name]

	# 	work_index -= 1
		
	# 	self.move_to_work_index(window_name, work_index)

	# def forward_work_each_worker(self, window_name):

	# 	game_object = self.game_object[window_name]
	# 	if game_object == None or game_object.main_scene == None:
	# 		return

	# 	work_name = game_object.current_schedule_work
	# 	if work_name != None:
	# 		try:
	# 			if not work_name in game_object.main_scene.move_status:
	# 				work_index = game_object.main_scene.last_status[work_name]
	# 			else:
	# 				work_index = game_object.main_scene.move_status[work_name]
	# 		except:
	# 			work_index = game_object.main_scene.last_status[work_name]

	# 	work_index += 1

	# 	self.move_to_work_index(window_name, work_index)

	# def callback_select_wlist_stringvar(self, args, option_name):
	# 	print('[DEBUG MoveStatus] callback_select_wlist_stringvar:', option_name, self.wlist_stringvar_skip_dic)

	# 	if len(option_name) < 1:
	# 		return

	# 	if not option_name in self.current_work_dic:
	# 		return

	# 	print('[DEBUG MoveStatus] CP1')
	# 	if option_name in self.wlist_stringvar_skip_dic:
	# 		if self.wlist_stringvar_skip_dic[option_name] == True:
	# 			self.wlist_stringvar_skip_dic[option_name] = False
	# 			print('[DEBUG MoveStatus] CPXX', self.wlist_stringvar_skip_dic)	
	# 			return

	# 	print('[DEBUG MoveStatus] CP2')	
	# 	if len(self.wlist_stringvar_dic[option_name].get()) < 1:
	# 		return

	# 	print('[DEBUG MoveStatus] CP3')
	# 	move_status = int(self.wlist_stringvar_dic[option_name].get().split('.')[0])
	# 	print('[DEBUG MoveStatus] moveStatus:', move_status, 'current_work_name', self.current_work_dic[option_name])

	# 	game_object = self.game_object[option_name]
	# 	if game_object == None or game_object.main_scene == None:
	# 		return

	# 	self.move_to_work_index(option_name, move_status)

	# def move_to_work_index(self, window_name, index):
	# 	if not window_name in self.game_object:
	# 		return

	# 	game_object = self.game_object[window_name]
	# 	if game_object == None or game_object.main_scene == None:
	# 		return

	# 	max_len = len(game_object.main_scene.get_game_config('schedule_list'))
	# 	if index >= max_len - 1:
	# 		index = max_len - 1

	# 	if index < 1:
	# 		index = 1

	# 	work_name = game_object.current_schedule_work
	# 	if work_name != None:
	# 		call_index = 0
	# 		if len(game_object.main_scene.callstack) > 0:
	# 			for each_call in game_object.main_scene.callstack:
	# 				iterator_key = game_object.build_iterator_key(call_index, each_call)
	# 				game_object.main_scene.set_option(iterator_key, None)
	# 				call_index += 1
	# 			game_object.main_scene.callstack.clear()
	# 		game_object.main_scene.callstack_status.clear()

	# 		game_object.main_scene.set_option(work_name + '_end_flag', True)
	# 		game_object.main_scene.move_status[work_name] = index

	# 	move_work_name = str(index) + '. ' + game_object.main_scene.get_game_config('schedule_list')[index-1]
	# 	self.wlist_stringvar_dic[window_name].set(move_work_name)

