import http.cookiejar
import urllib
import ssl
import logging
import json
from bs4 import BeautifulSoup
import time

http.client.HTTPConnection.debuglevel = 1


cj = http.cookiejar.CookieJar()
https_sslv23_handler = urllib.request.HTTPSHandler(context=ssl.SSLContext(ssl.PROTOCOL_SSLv23))

opener = urllib.request.build_opener(https_sslv23_handler, urllib.request.HTTPCookieProcessor(cj))
opener.addheaders=[('User-agent','Mozilla/5.0'), ('Accept-Language', 'ko-KR')]
urllib.request.install_opener(opener)

login_url = 'https://dogfooter.com/macro'
url = 'https://dogfooter.com/macro'

login_info = {
	'mb_id' : 'dogfooter_test',
	'mb_password' : 'wssong98',
	'mb_1' : '2018-02-08',
	'mb_3' : 'dogfooter'
}

login_request = urllib.parse.urlencode(login_info)

req = urllib.request.Request(login_url + '/bbs/login_check.php', login_request.encode('UTF-8'))
res = urllib.request.urlopen(req)

req = urllib.request.Request(login_url + '/dogfooter.php', login_request.encode('UTF-8'))
res = urllib.request.urlopen(req)

string = res.read().decode('utf-8')
soup = BeautifulSoup(string, 'html.parser')
# print(soup.prettify())
json_obj = json.loads(soup.prettify())
print(json_obj['dogfooter_test']['macro'], json_obj['dogfooter_test']['chatid'])
# print(soup.find(id='validation_check'))
# print(soup.find(id='ol_after_pt'))
# print(soup.find(id='dogfooter'))

# json_obj = json.loads(string)