import tkinter
import threading
import time
import collections
import likeyoubot_worker
import likeyoubot_gui
import likeyoubot_configure
import sys
import win32gui
import pickle
import os
import comtypes.client
import test_logingui

def connresize(e):
	global configure

	if e.width == configure.w and e.height == configure.h and e.x != 0 and e.y != 0:
		if e.x != configure.x or e.y != configure.y:
			configure.x = e.x
			configure.y = e.y
			try:
				with open(resource_path('lyb.cfg'), 'wb') as dat_file:
					pickle.dump(configure, dat_file)
			except:
				print('Exception: ', sys.exc_info()[0])


def resource_path(relative):
	return os.path.join(
	os.environ.get(
	    "_MEIPASS2",
	    os.path.abspath(".")
	),
	relative
	)

configure = None
root = tkinter.Tk()
root.resizable(width=False, height=False)
root.bind("<Configure>", connresize)

# TODO: 설정 파일 읽어오기
try:
	with open(resource_path('lyb.cfg'), 'rb') as dat_file:
		configure = pickle.load(dat_file)
		if configure.getGeometryLogin() == None:
			w = 320
			h = 140
			ws = root.winfo_screenwidth()
			hs = root.winfo_screenheight()
			x = (ws/2) - (w/2)
			y = (hs/2) - (h/2)
			configure.setGeometryLogin(w, h, x, y)

	root.geometry('%dx%d+%d+%d' % configure.getGeometryLogin())

	configure.path = resource_path('lyb.cfg')
	print('configure.path = ', configure.path)
except FileNotFoundError:
	w = 320
	h = 140
	ws = root.winfo_screenwidth()
	hs = root.winfo_screenheight()
	x = (ws/2) - (w/2)
	y = (hs/2) - (h/2)

	root.geometry('%dx%d+%d+%d' % (w, h, x, y))
	configure = likeyoubot_configure.LYBConfigure(x, y, w, h, 'MOMO|Nox', resource_path('lyb.cfg'))
	configure.setGeometryLogin(w, h, x, y)
	configure.setGeometry(800, 640, x, y)
	try:
		with open(resource_path('lyb.cfg'), 'wb') as dat_file:
			pickle.dump(configure, dat_file)
	except:
		print('Exception: ', sys.exc_info()[0])

except:
	print('Exception: ', sys.exc_info()[0])

configure.merge()
root.update()

print(root.winfo_width(), root.winfo_height())
# lyb_gui = likeyoubot_gui.LYBGUI(root, configure)
test_logingui.LYBLoginGUI(root, configure)

root.mainloop()



